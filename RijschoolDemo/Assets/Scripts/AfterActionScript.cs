﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AfterActionScript : MonoBehaviour {

    private List<Event> _oldScores;
    private List<Event> _newScores;
    private Scenario _activeScenario;
    public FillButtonScript button;
    public GameObject AfterActionObjectPrefab;
    public GameObject scrollview;

    // Use this for initialization
    void Start () {
        button.Locked = true;
        _activeScenario = FindObjectOfType<ScenarioContainer>().ActiveScenario;
        _newScores = FindObjectOfType<ScenarioContainer>().UpdatedDifficultyList;
        _oldScores = FindObjectOfType<ScenarioContainer>().OldScoreList;

        CreateAfterActionReport();

        SaveScores();
        
        button.onfillCompleted += ContinueButton;
        StartCoroutine(WaitAndUnlock());
        
    }
	
    void CreateAfterActionReport()
    {
        int validAverageCounter = 0;
        float difficultyTotal = 0f;
        for (int i = 0; i < _newScores.Count; i++)
        {
            if (_oldScores[i].EventType == Event.FrameEventType.ContinueButton)
                continue;
            GameObject newobject = Instantiate<GameObject>(AfterActionObjectPrefab, scrollview.transform);
            newobject.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            newobject.name = ("i = " + i + " en memory adress = " + newobject.GetInstanceID()).ToString();
            newobject.GetComponent<AfterActionProgressObject>().Initialize(_oldScores[i], _newScores[i]);
            validAverageCounter += 1;
            difficultyTotal += _newScores[i].Difficulty;
        }
        difficultyTotal = difficultyTotal / validAverageCounter;
        SaveGameDataBase.Instance.SetSkillAverageForScenario(_activeScenario.ID, difficultyTotal);
        //save this value for main menu;
    }

	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator WaitAndUnlock()
    {
        yield return new WaitForSeconds(2);
        button.Locked = false;
    }

    void SaveScores()
    {
        foreach (Event eEvent in _newScores)
        {
            SaveGameDataBase.Instance.SaveDifficulty(_activeScenario.ID, eEvent.ID, eEvent.Difficulty);
        }
    }

    public void ContinueButton(GameObject obj, string buttonName)
    {
        SceneManager.LoadSceneAsync("DynamicMenu");
    }
}
