﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class AnswerScript : MonoBehaviour {
    
    PopUpElementsScript UIElements;

    string questionText;
    public bool IsCorrect { get; set; }

	PopUpQuizScript quizParent;

    bool lockHighlight;
    
    public void SetAnswerInfo(QuizAnswer info, PopUpQuizScript quiz)
    {
		quizParent = quiz;

        UIElements = GetComponent<PopUpElementsScript>();
        UIElements.ButtonOverlay.gameObject.GetComponent<FillButtonScript>().onfillCompleted += AnswerSelected;

        questionText = info.AnswerText;
        IsCorrect = info.IsCorrect;

        Debug.Assert(UIElements != null);
        UIElements.MessageText.text = info.AnswerText;
    }

    public void AnswerSelected(GameObject obj, string objName)
    {
        quizParent.AnswerSelected(this);
    }

    public void HighlightAnswer()
    {
        if (lockHighlight) return;

        this.gameObject.transform.DOLocalMoveZ(-0.2f, 0.3f);
        SFXManager.instance.PlaySFX("hover");
    }

    public void UnHighlightAnswer()
    {
        if (lockHighlight) return;

        this.gameObject.transform.DOLocalMoveZ(0.0f, 0.3f);
    }

	public void VisualizePopOut(bool chosen){
        if (chosen)
        {
            UIElements.LongBG.GetComponent<Image>().color = (IsCorrect) ? new Color(0.6156862745098039f, 0.7647058823529412f, 0.3764705882352941f) : new Color(0.7647058823529412f, 0.3764705882352941f, 0.3764705882352941f);
            string sfx = "";
            if (IsCorrect)
                sfx = "completedPopUp";
            else
                sfx = "wrongAnswer";

            SFXManager.instance.PlaySFX(sfx);

            gameObject.transform.DOLocalMoveZ(-0.3f, 0.3f);
        } else
            gameObject.transform.DOLocalMoveZ(0.1f, 0.3f);

        lockHighlight = true;
        
	}

}
