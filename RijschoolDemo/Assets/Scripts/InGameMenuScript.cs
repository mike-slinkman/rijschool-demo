﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenuScript : MonoBehaviour {

    //TODO test with new videoplayerscript and activate again?

    public static InGameMenuScript instance = null;

    [SerializeField]
    private List<FillButtonScript> buttons = new List<FillButtonScript>();
    private bool isPaused;
    
    // Use this for initialization
    void Awake () {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        Debug.Assert(buttons.Count == 1);

        foreach (FillButtonScript button in buttons)
            button.onfillCompleted += ButtonPressed;
        
        //buttons[0].gameObject.SetActive(false);
        //buttons[1].gameObject.SetActive(true);
    }

    public void FadeButtons(bool turnActive)
    {
        Debug.Log("buttons action called");
        foreach (FillButtonScript button in buttons)
            button.gameObject.SetActive(turnActive);
    }
	
	public void ButtonPressed(GameObject obj, string buttonName)
    {
        switch (buttonName)
        {
            case "":
                //nothing
                break;
            case "PlayBtn":
                //buttons[0].gameObject.SetActive(false);
                //buttons[1].gameObject.SetActive(true);
                VideoPlayerScript.instance.ContinueVideo();
                break;
            case "PauseBtn":
                //buttons[0].gameObject.SetActive(true);
                //buttons[1].gameObject.SetActive(false);
                VideoPlayerScript.instance.PauseVideo();
                break;
            case "ReplayBtn":
                SceneManager.LoadSceneAsync(0);
                ///restart scene
                break;
            default:
                Debug.LogWarning("WARNING: Unknown button pressed. Misspelled button name? Gr Boy!");
                break;
        }
    }


}
