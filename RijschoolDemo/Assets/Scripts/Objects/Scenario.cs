﻿using Newtonsoft.Json;
using System.Collections;
using UnityEngine;

public class Scenario {

    public int ID { get; set; }
    public string Name { get; set; }
    public string VideoName { get; set; }
    [JsonIgnore]
    public EventDataBase EventDB { get; set; }
    public string EventDBID { get; set; }
    [JsonIgnore]
    public PositionDataDataBase PositionDB { get; set; }
    public string PositionDBID { get; set; }
    public bool IsDownloaded { get; set; }
    [JsonIgnore]
    private bool downloading = false;


    /// <summary>
    /// empty constructor for Json.net deserializer. Do not use 
    /// </summary>
    public Scenario()
    {

    }


    /// <summary>
    /// Create a new scenario. oID should be retrieved from ScenarioDataBase.
    /// </summary>
    /// <param name="oID">ID for this Scenario. Should be retrieved from ScenarioDataBase.</param>
    /// <param name="oName">Name for this Scenario</param>
    public Scenario(int oID, string oName)
    {
        ID = oID;
        Name = oName;
        if (ID != 0)
        {
            EventDBID = ID.ToString() + "_E_" + Name;
            EventDB = new EventDataBase(EventDBID);
            PositionDBID = ID.ToString() + "_P_" + Name;
        }
        IsDownloaded = false;
    }

    /// <summary>
    /// Loads scenario information and loads scene to play it in.
    /// </summary>
    public IEnumerator Play()
    {
        Debug.Log("Play is called on scenario: " + Name);
        //downloading is set to true. the DownloadManager.instance.Download calls a delegate when done that sets downloading back to false. this way the downloads wont continue untill the previous one is done.
        //download video
        downloading = true;
        Debug.Log("starting download for video: " + VideoName);
        DownloadManager.instance.Download(DownloadManager.DownloadType.Video, VideoName, DownloadDone);
        yield return new WaitUntil(() => !downloading);

        //download EventDB
        downloading = true;
        Debug.Log("starting download for events: " + EventDBID);
        DownloadManager.instance.Download(DownloadManager.DownloadType.Events, EventDBID, DownloadDone);
        yield return new WaitUntil(() => !downloading);

        
        //download mirrorpositions
        downloading = true;
        Debug.Log("starting download for positions: " + PositionDBID);
        DownloadManager.instance.Download(DownloadManager.DownloadType.Positions, PositionDBID, DownloadDone);
        yield return new WaitUntil(() => !downloading);
        

        Debug.Log("Starting Scenario");
        EventDB = new EventDataBase(EventDBID);
        GameObject.FindObjectOfType<ScenarioContainer>().ActiveScenario = this;
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("PlayScene");
        yield return new WaitForEndOfFrame();
    }
    
    /// <summary>
    /// Call when not sure if EventDB is loaded.
    /// </summary>
    public void LoadEventDB()
    {
        EventDB = new EventDataBase(EventDBID);
    }

    /// <summary>
    /// Call when not sure if PositionDB is loaded.
    /// </summary>
    public void LoadPositionsDB()
    {
        PositionDB = new PositionDataDataBase(PositionDBID);
    }

    /// <summary>
    /// Used for callbacks from the downloader.
    /// </summary>
    public void DownloadDone()
    {
        Debug.Log("Downloading done!");
        downloading = false;
    }
}
