﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public class RijschoolMenuItem {

    public enum SelectAction
    {
        MenuLayer,
        PlayVideo,
        PlayStill
    }

    public int ID;
    public int parentID;
    public List<int> children = new List<int>();
    public SelectAction selectAction;
    public bool IsRoot { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Icon { get; set; }
    [JsonIgnore]
    private Sprite _iconSprite;
    [JsonIgnore]
    public Sprite IconSprite {
        get
        {
            if (_iconSprite == null)
                LoadImage();
            return _iconSprite;
        }
        set { _iconSprite = value; }
    }
    public int ScenarioID { get; set; }
    
    public void OnSelect(MonoBehaviour monoBehaviour)
    {
        switch (selectAction)
        {
            case SelectAction.MenuLayer:
                GameObject.FindObjectOfType<MenuManager>().BuildMenu(this);
                break;
            case SelectAction.PlayVideo:
                ScenarioDataBase dataBase = ScenarioDataBase.Instance;
                Debug.Log("selected scenario: " + dataBase.GetScenarioByID(ScenarioID));
                monoBehaviour.StartCoroutine(dataBase.GetScenarioByID(ScenarioID).Play());
                break;
            case SelectAction.PlayStill:
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Sets the specified object as child.
    /// </summary>
    /// <param name="pChild">Object to be the new child.</param>
    public void SetChild(RijschoolMenuItem pChild)
    {
        children.Add(pChild.ID);
        pChild.parentID = ID;
    }

    /// <summary>
    /// Removes the specified object as child.
    /// </summary>
    /// <param name="pChild">Child object to remove.</param>
    public void RemoveChild(RijschoolMenuItem pChild)
    {
        children.Remove(pChild.ID);
        pChild.parentID = 0;
    }

    /// <summary>
    /// Check if the object has a parent.
    /// </summary>
    /// <returns>True when parent is not null.</returns>
    public bool HasParent()
    {
        if (parentID != 0)
            return true;
        return false;
    }

    /// <summary>
    /// Check if object has children.
    /// </summary>
    /// <returns>True when childlist is not empty.</returns>
    public bool HasChildren()
    {
        if (children.Count > 0)
            return true;
        return false;
    }

    public void RemoveAllChildren()
    {
        for (int i = children.Count - 1; i >= 0; i--)
            RemoveChild(MenuDataBase.Instance.GetMenuItemByID(children[i]));
    }

    private void LoadImage()
    {

        string _filePath = "";
        _filePath = Path.Combine(Application.persistentDataPath, "Images");
        _filePath = Path.Combine(_filePath, Icon + ".png");

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(_filePath))
        {
            fileData = File.ReadAllBytes(_filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            IconSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        }
        else
            IconSprite = null;
    }

}
