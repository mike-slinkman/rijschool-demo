﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Event : ICloneable
{
    public int ID = 0;
    public List<QuizAnswer> QuizAnswers { get; set; }
    public List<Overlay> GoalList { get; set; }
    public int PresetID { get; set; }
    [JsonIgnore]
    public Preset pPreset { get; set; }
    //or preset;
    public int Frame { get; set; }
    public int EndFrame { get; set; }
    public bool FreezeVideo { get; set; }
    public bool IsPopUp { get; set; }

    public string ImageName { get; set; }
    public string Header { get; set; }
    public string Message { get; set; }
    public bool Button { get; set; }

    public int SkillThresholdMedium { get; set; }
    public int SkillThresholdHard { get; set; }
    public float MediumTimeLimit { get; set; }
    [JsonIgnore]
    protected float timer;

    [JsonIgnore]
    public int Difficulty { get; set; }
    [JsonIgnore]
    public int CurrentStage { get; set; }
    [JsonIgnore]
    string goal = "";
    [JsonIgnore]
    public bool MarkedForDeletion { get; set; }
    [JsonIgnore]
    private AbstractPopUp popUp;

    [JsonIgnore]
    bool hasPausedVideo = false;

    public enum FrameEventType
    {
        Custom,
        Quiz,
        ContinueButton,
        StillGame,
        Preset
    };

    /// <summary>
    /// what is the event type of this frame event
    /// </summary>
    public FrameEventType EventType { get; set; }

    /// <summary>
    /// constructor for event frame
    /// </summary>
    /// <param name="should frames freeze?"></param>
    /// <param name="should a message pop up?"></param>
    /// <param name="should the popup have an image?"></param>
    /// <param name="what is the header text?"></param>
    /// <param name="what is the message text?"></param>
    /// <param name="should there be a button?"></param>
    public Event(int iD, int frame, List<QuizAnswer> qQ = null, FrameEventType eventType = FrameEventType.Custom, bool freezeEvent = true, bool popUp = true, string image = "", string header = "", string message = "", bool button = true, int stage = 0)
    {
        ID = iD;
        Frame = frame;
        QuizAnswers = qQ;
        if (qQ == null) QuizAnswers = new List<QuizAnswer>();
        EventType = eventType;
        FreezeVideo = freezeEvent;
        IsPopUp = popUp;
        ImageName = image;
        Header = header;
        Message = message;
        Button = button;
        CurrentStage = stage;
        if (GoalList == null) GoalList = new List<Overlay>();
        PresetID = 0;
        MediumTimeLimit = 20f;
    }

    /// <summary>
    /// Activates the event and shows popup if required.
    /// </summary>
    public void Activate()
    {
        timer = MediumTimeLimit;
        if (timer == 0)
            timer = 20f;
        
        MarkedForDeletion = false;
        if (pPreset == null)
        {
            pPreset = PresetDataBase.Instance.GetPresetByID(PresetID);
            Debug.Log("Preset loaded.");
        }
        if (SkillThresholdMedium == 0) SkillThresholdMedium = 20;
        if (SkillThresholdHard == 0) SkillThresholdHard = 60;
        if (EventType == FrameEventType.StillGame)
        {
            //still game code goes here
            popUp = PopupManager.instance.CreatePopup(this);
        }
        else if (EventType == FrameEventType.Quiz)
        {
            //quiz popup;
            //pause video;
            popUp = PopupManager.instance.CreatePopup(this);
            PauseVideo();
        }
        else if (EventType == FrameEventType.ContinueButton)
        {
            //button popup;
            //pause video;
            popUp = PopupManager.instance.CreatePopup(this);
            PauseVideo();
        }
        else
        {
            if (Difficulty < SkillThresholdMedium)
            {
                //show popup
                //pause video
                Debug.Log("skill level " + Difficulty + " under medium treshold (" + SkillThresholdMedium + ")");
                popUp = PopupManager.instance.CreatePopup(this);
                PauseVideo();

            }
            else if (Difficulty < SkillThresholdHard)
            {
                Debug.Log("skill level " + Difficulty + " under hard treshold (" + SkillThresholdMedium + ")");
                //dont show popup, pause video
                PauseVideo();
            }
            else
            {
                Debug.Log("highest difficulty reached!");
                //dont show popup, dont pause video
                //TODO
            }
        }
    }
    
    /// <summary>
    /// Tell pop up that user has looked at overlay, check if this matches the goal.
    /// </summary>
    public void CheckForGoals(string target)
    {
        if (EventType == Event.FrameEventType.Quiz ||
            EventType == Event.FrameEventType.ContinueButton) return;

        if (EventType != FrameEventType.Preset && GoalList == null ||
            EventType != FrameEventType.Preset && GoalList.Count < 1 ||
            EventType == FrameEventType.Preset && pPreset.GoalList == null ||
            EventType == FrameEventType.Preset && pPreset.GoalList.Count < 1)
        {
            return;
        }

        if ((EventType != FrameEventType.Preset && target == GoalList[CurrentStage].Name) ||
            (EventType == FrameEventType.Preset && target == pPreset.GoalList[CurrentStage].Name))
        {
            if (EventType != FrameEventType.Preset && GoalList != null && GoalList.Count != 0 && GoalList[CurrentStage] != null)
                GoalList[CurrentStage].HasLookedAt = true;
            if (EventType == FrameEventType.Preset && pPreset != null && pPreset.GoalList.Count != 0 && pPreset.GoalList[CurrentStage] != null)
                pPreset.GoalList[CurrentStage].HasLookedAt = true;
            //looked at right target
            CurrentStage++;
            SFXManager.instance.PlaySFX("completedPopUp");
            if ((EventType != FrameEventType.Preset && CurrentStage >= GoalList.Count) ||
                (EventType == FrameEventType.Preset && CurrentStage >= pPreset.GoalList.Count))
            {
                //all tasks are done.
                Done(true);
            }
        }
        else
        {
            if (EventType != FrameEventType.Preset)
            {
                for (int i = GoalList.Count; i > 0; i--)
                {
                    if (GoalList[i - 1].Name == target && !GoalList[i - 1].HasLookedAt)
                    {
                        GoalList[i - 1].HasLookedAt = true;
                        break;
                    }
                }
            }
            else
            {
                for (int i = pPreset.GoalList.Count; i > 0; i--)
                {
                    if (pPreset.GoalList[i - 1].Name == target && !pPreset.GoalList[i - 1].HasLookedAt)
                    {
                        pPreset.GoalList[i - 1].HasLookedAt = true;
                        break;
                    }
                }
            }
        }
        if (EventType == FrameEventType.Custom)
        {
            //check if all got looked at
            bool alldone = true;
            for (int i = GoalList.Count; i > 0; i--)
            {
                if (!GoalList[i - 1].HasLookedAt)
                {
                    alldone = false;
                }
            }
            if (alldone)
            {
                //all looked at but in wrong order. lets do something maybe
            }
        }
        else
        {
            //check if all got looked at
            bool alldone = true;
            for (int i = pPreset.GoalList.Count; i > 0; i--)
            {
                if (!pPreset.GoalList[i - 1].HasLookedAt)
                {
                    alldone = false;
                }
            }
            if (alldone)
            {
                //all looked at but in wrong order. lets do something maybe
            }
        }
    }

    public void Tick(float deltatime)
    {
        timer -= deltatime;
        Debug.Log(timer);
        if (timer <= 0 && Difficulty > SkillThresholdMedium)
        {
            Done(false);
        }
    }

    /// <summary>
    /// Changes the difficulty based on the result of this event.
    /// </summary>
    /// <param name="passed">Was the event passed or not?</param>
    public void DoDifficultyAdjustment(bool passed = false)
    {
        int skillChange = 0;
        if (EventType == FrameEventType.Custom)
        {
            skillChange = (passed) ? 10 : -5;
        }
        if (EventType == FrameEventType.Quiz)
        {
            skillChange = (passed) ? 20 : -25;
        }
        Difficulty += skillChange;
        Difficulty = Mathf.Clamp(Difficulty, 0, 100);
    }

    private void PauseVideo()
    {
        VideoPlayerScript.instance.PauseVideo();
        hasPausedVideo = true;
    }

    /// <summary>
    /// Ends the event.
    /// </summary>
    /// <param name="passed">Did we pass the event?</param>
    public void Done(bool passed = false)
    {
        DoDifficultyAdjustment(passed);
        if (popUp != null && !popUp.MarkedForDeletion)
            popUp.Remove(true);
        if (VideoPlayerScript.instance != null && hasPausedVideo)
            VideoPlayerScript.instance.ContinueVideo(this);

        MarkedForDeletion = true;
    }

    public object Clone()
    {
        return this.MemberwiseClone();
    }
}