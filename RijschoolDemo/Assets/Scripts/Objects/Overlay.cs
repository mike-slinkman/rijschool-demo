﻿using Newtonsoft.Json;

public class Overlay {
    public int ID;
    public string Name;
    [JsonIgnore]
    public PositionData positions;
    [JsonIgnore]
    public bool HasLookedAt = false;
}
