﻿using UnityEngine;

public class PositionData {
    public int OverlayID { get; set; }
    public Vector3 RectTransform { get; set; }
    public Vector2 RectDimentions { get; set; }
    public Quaternion Rotation { get; set; }
    public Vector3 Scale { get; set; }
}
