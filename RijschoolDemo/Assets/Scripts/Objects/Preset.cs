﻿using System.Collections.Generic;

public class Preset {

    public int ID;
    public string Name;
    public List<Overlay> GoalList = new List<Overlay>();
    public int Counter;

    public Preset(int oID)
    {
        ID = oID;
    }
}
