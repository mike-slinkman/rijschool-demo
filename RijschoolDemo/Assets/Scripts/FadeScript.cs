﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Script that handles the fading for the fadeObject prefab
/// Add the prefab to the main camera.
/// </summary>
public class FadeScript : MonoBehaviour
{
    /// <summary>
    /// Starts the co-routine that fades into the scene
    /// </summary>

    private void Start()
    {
        FadeInScene();
    }

    public void FadeInScene()
    {
        Debug.Log("Starting the coroutine");
        StartCoroutine(FadeInCanvas());
    }

    /// <summary>
    /// Starts the co-routine that fades out the scene
    /// </summary>
    public void FadeOutScene()
    {
        StartCoroutine(FadeOutCanvas());
    }

    /// <summary>
    /// Co-routine that handles the fading into the scene
    /// </summary>
    IEnumerator FadeInCanvas()
    {
        CanvasGroup canvasGroup = GetComponent<CanvasGroup>();

        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime / 2;
            yield return null;
        }
        canvasGroup.interactable = false;
        yield return null;
    }

    /// <summary>
    /// Co-routine that handles the fading out of the scene
    /// </summary>
    IEnumerator FadeOutCanvas()
    {
        CanvasGroup canvasGroup = GetComponent<CanvasGroup>();

        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime / 2;
            yield return null;
        }
        canvasGroup.interactable = false;
        yield return null;
    }
}