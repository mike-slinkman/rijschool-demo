﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using DG.Tweening;

public class NewMenuController : MonoBehaviour {

    [SerializeField]
    bool startFill;
    [SerializeField]
    Image fillBar;
    [SerializeField]
    GameObject buttons;
    [SerializeField]
    GameObject downloadWindow;
    [SerializeField]
    GameObject instructionsWindow;
    //[SerializeField]
    //DownloadManager downloadManager;
    [SerializeField]
    FadeScript fade;

    bool isDownloading;
    [SerializeField]
    string buttonType;

    void Awake()
    {
        isDownloading = false;    
    }

    void Start() {

    }

    void Update () {
        if (startFill && fillBar != null)
        {
            fillBar.fillAmount += Time.deltaTime * 0.7f;
            if (fillBar.fillAmount >= 1)
            {
                switch (buttonType) {
                    case "play":
                        StartCoroutine(playBtnPressed());
                        break;
                    case "instructions":
                        StartCoroutine(instructionsBtnPressed());
                        break;
                    case "instructionsDone":
                        StartCoroutine(instructionsDoneBtnPressed());
                        break;
                }
            }
        }
	}

    public void OnGazeOver()
    {
        startFill = true;
    }

    public void OnGazeOut()
    {
        startFill = false;
        fillBar.fillAmount = 0;
    }

    public void DownloadDone() {
        StartCoroutine(LoadNewScene());
    }

    IEnumerator LoadNewScene() {
        fade.FadeOutScene();
        yield return null;

        AsyncOperation async = SceneManager.LoadSceneAsync("1.1.A");
        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            float progress = Mathf.Clamp01(async.progress / 0.9f);

            if (Mathf.Approximately(async.progress, 0.9f))
            {
                Debug.Log("Starting");
                async.allowSceneActivation = true;
            }

            yield return null;
        }
    }

    IEnumerator playBtnPressed()
    {
        buttons.gameObject.transform.DOLocalMoveX(-2000f, 3.0f);
        downloadWindow.SetActive(true);
        downloadWindow.gameObject.transform.DOLocalMoveX(0.0f, 3.0f);

        //if (downloadManager != null && !isDownloading)
        //{
        //    downloadManager.startCheck();
        //    isDownloading = true;
        //}

        yield return new WaitForSeconds(3.0f);
        buttons.SetActive(false);
    }

    IEnumerator instructionsBtnPressed()
    {
        buttons.gameObject.transform.DOLocalMoveX(-2000f, 3.0f);
        instructionsWindow.SetActive(true);
        instructionsWindow.gameObject.transform.DOLocalMoveX(0.0f, 3.0f);
        yield return new WaitForSeconds(3.0f);
        buttons.SetActive(false);
    }

    IEnumerator instructionsDoneBtnPressed()
    {
        buttons.gameObject.transform.DOLocalMoveX(0.0f, 3.0f);
        buttons.SetActive(true);
        instructionsWindow.transform.DOLocalMoveX(2000.0f, 3.0f);
        yield return new WaitForSeconds(3.0f);
        instructionsWindow.SetActive(false);

    }
}
