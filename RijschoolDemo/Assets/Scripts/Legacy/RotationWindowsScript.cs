﻿
using UnityEngine;

public class RotationWindowsScript : MonoBehaviour {

    private float speed = 5.0f;
    private float xRot;
    private float yRot;

    void Awake()
    {
        Cursor.visible = false;
    }

    private void Update()
    {
        xRot += Input.GetAxis("Mouse Y") * -speed;
        yRot += Input.GetAxis("Mouse X") * speed;

        this.transform.eulerAngles = new Vector3(xRot, yRot, 0);
   } 
}
