﻿using System.Collections.Generic;

public class EventFrames {

    public Dictionary<int, Event> GetEventFrames (int scene) {

        //the events as a dictionary
        Dictionary<int, Event> eventFrames = new Dictionary<int, Event>();


        /// ------------------------------------ ///
        ///              scenario 1              ///
        /// ------------------------------------ ///

        /*switch (scene)
        {
            case 1:
                // Script 25: Afslaan
                eventFrames.Add(135, new Event(
                    135,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "d05l",
                    "Afslaan",
                    @"Welkom bij de les over Afslaan. 
In dit script behandelen we alle handelingen die je moet verrichten voor je af mag slaan. ",
                    true
                    ));

                eventFrames.Add(418, new Event(
                    418,
                    null,
                    Event.FrameEventType.FarLeft,
                    true,
                    true,
                    "",
                    "Scan",
                    "Op de kruising gaan we links afslaan. Scan de richting waarin je wilt gaan rijden.",
                    false
                    ));

                eventFrames.Add(442, new Event(
                    442,
                    null,
                    Event.FrameEventType.FarRight,
                    true,
                    true,
                    "",
                    "Scan",
                    "Vergeet niet om de weg van rechts te controleren.",
                    false
                    ));

                eventFrames.Add(466, new Event(
                    466,
                    new List<QuizAnswer>()
                    {
                new QuizAnswer("Nee", false),
                new QuizAnswer("Ja", true)
                    },
                    Event.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "Voorrang",
                    "Moet je de auto die van rechts komt voor laten gaan?",
                    false
                    ));

                eventFrames.Add(500, new Event(
                    500,
                    null,
                    Event.FrameEventType.TurnLeft,
                    true,
                    true,
                    "",
                    "Afslaan",
                    "Kijk binnenspiegel, voor, linkerbuitenspiegel, over linkerschouder.",
                    false
                    ));

                /*eventFrames.Add(512, new FrameEvent(
                    512,
                    new List<QuizAnswer>()
                    {
                new QuizAnswer("Ja", true),
                new QuizAnswer("Nee", false)
                    },
                    FrameEvent.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "Veiligheid",
                    "Is het nu veilig om af te slaan?",
                    false
                    ));*//*

                eventFrames.Add(524, new Event(
                    524,
                    null,
                    Event.FrameEventType.FarLeft,
                    true,
                    true,
                    "",
                    "Scan",
                    "Kijk in de te volgen richting. Pas je snelheid aan aan het verkeer voor en achter je.",
                    false
                    ));

                eventFrames.Add(625, new Event(
                    625,
                    null,
                    Event.FrameEventType.ExitTurn,
                    true,
                    true,
                    "",
                    "Scan",
                    "na het uitrijden van de bocht moet je weer een overzicht krijgen van wat er achter je gebeurt. Kijk daarom in de binnenspiegel, dan in de linker buitenspiegel",
                    false
                    ));
                break;

            case 2:
                // Script 10: Scan

                eventFrames.Add(135, new Event(
                    135,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "b1",
                    "Scannen",
                    @"Welkom bij de les over Scannen. 
Door goed te scannen krijg je overzicht van de verkeersomgeving en kun je goed vooruit denken en handelen. Je bent dan in staat aangepast en besluitvaardig auto te rijden.",
                    true
                    ));

                eventFrames.Add(500, new Event(
                    500,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "",
                    "Kijk vooruit",
                    "Kijk minimaal 200 meter vooruit en ook verder als dit nodig en mogelijk is.",
                    true
                    ));

                eventFrames.Add(750, new Event(
                    750,
                    null,
                    Event.FrameEventType.FarWide,
                    true,
                    true,
                    "",
                    "Niet fixeren",
                    "Alles wat buiten je bewuste waarnemingsgebied valt, wordt niet door je hersenen verwerkt. Je moet dus je ogen en hoofd bewegen om goed te kunnen scannen. Kijk links naar buiten, daarna rechts naar buiten.",
                    false
                    ));

                eventFrames.Add(1000, new Event(
                    1000,
                    null,
                    Event.FrameEventType.MidMirror,
                    true,
                    true,
                    "",
                    "Binnenspiegel",
                    "Ook achter je gebeuren dingen waar je rekening mee moet houden Als je goed de binnenspiegel gebruikt, zie je eerder een gevaarlijke situatie ontstaan en kun je er op inspelen. Kijk nu in de binnenspiegel.",
                    false
                    ));

                eventFrames.Add(1250, new Event(
                    1250,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "",
                    "Kijk voor je",
                    "Het in de verte kijken heeft tot gevolg dat je informatie vlak voor de auto overslaat. Om dit te voorkomen, moet je ook regelmatig bewust dicht voor de auto kijken.",
                    true
                    ));

                eventFrames.Add(1500, new Event(
                    1500,
                    null,
                    Event.FrameEventType.LeftMirror,
                    true,
                    true,
                    "",
                    "Kijk in de buitenspiegels",
                    "Het kan zijn dat een voertuig niet goed zichtbaar is in de binnenspiegel, bijvoorbeeld als het snelheidsverschil groot is of als het andere voertuig erg smal is. je buitenspiegels zijn dan extra belangrijk. Kijk in je linkerbuitenspiegel.",
                    false
                    ));

                eventFrames.Add(1750, new Event(
                    1750,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "",
                    "Scancyclus",
                    "Het komt er in de praktijk op neer dat je iedere 5 tot 8 seconden deze routine moet herhalen. Kijk 200m. vooruit, blik niet fixeren, kijk in spiegels, kijk voor je, kijk in buitenspiegels.",
                    true
                    ));

                break;
            case 3:
                // Script 30: Rijstrook wisselen
                
                eventFrames.Add(135, new Event(
                    135,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "g03",
                    "Rijstrook wisselen",
                    @"Welkom bij de les over Rijstroken wisselen en zijdelings verplaatsen.
In deze les gaan we een vrachtwagen inhalen. Hiervoor moeten we twee keer van rijstrook wisselen. ",
                    true
                    ));
                
                eventFrames.Add(300, new Event(
                    300,
                    null,
                    Event.FrameEventType.MergeLeft,
                    true,
                    true,
                    "",
                    "Scannen",
                    "Voor je een zijdelingse verplaatsing naar links maakt, kijk je eerst in je binnenspiegel, daarna in je linkerbuitenspiegel en tot slot over je linkerschouder. Probeer dit nu in die volgorde.",
                    false
                    ));

                 eventFrames.Add(372, new Event(
                     372,
                     null,
                     Event.FrameEventType.MergeLeft,
                     true,
                     true,
                     "",
                     "Scannen",
                     "Na het aangeven van je richting doe je deze handelingen nogmaals. Kijk eerst in je binnenspiegel, daarna in je linkerbuitenspiegel en tot slot over je linkerschouder.",
                     false
                     ));

                eventFrames.Add(668, new Event(
                    668,
                    null,
                    Event.FrameEventType.MergeRight,
                    true,
                    true,
                    "",
                    "Scannen",
                    "Voor je een zijdelingse verplaatsing naar rechts maakt, kijk je eerst in je binnenspiegel, daarna in je rechterbuitenspiegel en tot slot over je rechterschouder. Probeer dit nu in die volgorde.",
                    false
                    ));

                eventFrames.Add(740, new Event(
                    740,
                    null,
                    Event.FrameEventType.MergeRight,
                    true,
                    true,
                    "",
                    "Scannen",
                    "Herhaal de zojuist gemaakte handelingen nog een keer zodat je zeker weet dat je veilig van rijbaan kan wisselen.",
                    false
                    ));

                break;
            case 4:
                // minirotonde 

                eventFrames.Add(130, new Event(
                    130,
                    null,
                    Event.FrameEventType.StartScenario,
                    false,
                    false
                    ));

                eventFrames.Add(135, new Event(
                    135,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "d01",
                    "MINIROTONDE",
                    @"In deze les gaan we leren waar we bij minirotondes op moeten letten. 
Als je niet goed oplet of voorrang geeft, kunnen minirotondes gevaarlijke situaties opleveren.",
                    true
                    ));

                eventFrames.Add(361, new Event(
                    361,
                    null,
                    Event.FrameEventType.NearRoundAbout,
                    true,
                    true,
                    "",
                    "MINIROTONDE",
                    @"Kijk bij het naderen van rotondes altijd eerst in de binnenspiegel, daarna voor je en vervolgens in de linkerbuitenspiegel en tot slot de rechterbuitenspiegel.
probeer dit nu.",
                    false
                    ));

                eventFrames.Add(380, new Event(
                    380,
                    new List<QuizAnswer>()
                    {
                        new QuizAnswer("Geen richting aangeven", false),
                        new QuizAnswer("Richting naar rechts aangeven.", false),
                        new QuizAnswer("Richting naar links aangeven.", true)
                    },
                    Event.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "MINIROTONDE",
                    "We nemen de derde afslag (linksaf) op de rotonde, Welke richting moeten we aangeven?",
                    false
                    ));

                eventFrames.Add(658, new Event(
                    658,
                    new List<QuizAnswer>()
                    {
                        new QuizAnswer("Ja.", true),
                        new QuizAnswer("Nee.", false)
                    },
                    Event.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "MINIROTONDE",
                    "Moet je hier voorrang verlenen aan de van links komende fietser?",
                    false
                    ));

                eventFrames.Add(1200, new Event(
                    1200,
                    null,
                    Event.FrameEventType.MergeRight,
                    true,
                    true,
                    "",
                    "MINIROTONDE",
                    "Bij het verlaten van de rotonde verleen je voorrang aan al het andere verkeer op de rotonde. Kijk daarom nu eerst goed in de binnenspiegel, daarna in de rechterbuitenspiegel en tot slot in over je rechterschouder.",
                    false
                    ));


                break;
            case 5:
                // wegen

                eventFrames.Add(130, new Event(
                    130,
                    null,
                    Event.FrameEventType.StartScenario,
                    false,
                    false
                    ));

                eventFrames.Add(135, new Event(
                    135,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "g03",
                    "WEGEN",
                    @"Deze situatie speelt zich af op de autosnelweg. We leren hier hoe we veilig een snelweg verlaten. Dit gaan we bij de eerstvolgende uitrijstrook doen.",
                    true
                    ));

                eventFrames.Add(841, new Event(
                    841,
                    new List<QuizAnswer>()
                    {
                        new QuizAnswer("100 meter voor de uitrijstrook.", false),
                        new QuizAnswer("200 meter voor de uitrijstrook.", false),
                        new QuizAnswer("300 meter voor de uitrijstrook.", true)
                    },
                    Event.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "WEGEN",
                    "Hoeveel meter voor de uitrijstrook moet je beginnen met richting aangeven?",
                    false
                    ));

                eventFrames.Add(1080, new Event(
                    1080,
                    null,
                    Event.FrameEventType.MergeRight,
                    true,
                    true,
                    "",
                    "WEGEN",
                    "300 meter voor de uitrijstrook geef je richting en kijk je meteen al of het veilig is om af te slaan. Kijk hiervoor nu eerst in je binnenspiegel, vervolgens in de rechterbuitenspiegel en tot slot over je rechterschouder.",
                    false
                    ));

                eventFrames.Add(1500, new Event(
                    1500,
                    null,
                    Event.FrameEventType.MergeRight,
                    true,
                    true,
                    "",
                    "WEGEN",
                    "Bij het afslaan herhalen we de kijkacties nogmaals. Kijk nu dus weer eerst in je binnenspiegel, vervolgens in de rechterbuitenspiegel en tot slot over je rechterschouder.",
                    false
                    ));

                eventFrames.Add(1853, new Event(
                    1853,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "",
                    "WEGEN",
                    "Ter hoogte van het UIT bord mag de richtingaanwijzer pas weer uit.",
                    true
                    ));

                eventFrames.Add(2100, new Event(
                    2100,
                    new List<QuizAnswer>()
                    {
                        new QuizAnswer("Niets doen en dezelfde snelheid behouden. ", false),
                        new QuizAnswer("Gas loslaten en langzaam snelheid verminderen. ", true),
                        new QuizAnswer("Remmen en de snelheid sterk verminderen.", false)
                    },
                    Event.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "WEGEN",
                    "Wat doen we in deze situatie met onze snelheid",
                    false
                    ));


                break;
            default:
                //error
                
                eventFrames.Add(130, new Event(
                    130,
                    null,
                    Event.FrameEventType.StartScenario,
                    false,
                    false
                    ));

                eventFrames.Add(135, new Event(
                    135,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "WhiteOnTransparent",
                    "Welkom bij Virtual Reality rijlessen",
                    "Hier demonstreren we wat er mogelijk is in VR. Deze demo pauzeert op belangrijke momenten om je de tijd te geven om te reageren op wat er gebeurt. Je kunt om je heen kijken door je hoofd te bewegen en op knoppen 'klikken' door er naar te kijken met het blauwe mikpunt.\nProbeer het eens!",
                    true
                    ));

                eventFrames.Add(748, new Event(
                    748,
                    null,
                    Event.FrameEventType.FarWide,
                    true,
                    true,
                    "",
                    "Breed kijken",
                    "Scan altijd breed om je heen. \n\nKijk eerst wijd naar links en daarna wijd naar rechts.",
                    false
                    ));

                eventFrames.Add(955, new Event(
                    955,
                    null,
                    Event.FrameEventType.MidMirror,
                    true,
                    true,
                    "",
                    "Spiegels checken",
                    "Kijk regelmatig in je binnenspiegel. Probeer zo'n elke 5 tot 10 seconden te controleren wat er achter je gebeurt.\n\n Kijk nu in je binnenspiegel.",
                    false
                    ));

                eventFrames.Add(1144, new Event(
                    1144,
                    null,
                    Event.FrameEventType.ContinueButton,
                    true,
                    true,
                    "",
                    "Bochten inkijken",
                    "Bij bochten is het de bedoeling dat je zo ver mogelijk de bocht in kijkt, om gevaren op tijd aan te zien komen.\n\nFocus je weer ongeveer op de afstand tot de auto voor je.",
                    true
                    ));

                eventFrames.Add(1525, new Event(
                    1525,
                    null,
                    Event.FrameEventType.EndScenario,
                    false,
                    false
                    ));

                eventFrames.Add(1550, new Event(
                    1550,
                    new List<QuizAnswer>()
                    {
                new QuizAnswer("Blauw", false),
                new QuizAnswer("Wit", true),
                new QuizAnswer("groen", false)
                    },
                    Event.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "Quizvraag - Gevaarherkenning",
                    "Welke kleur was de auto, die jou net gevaarlijk hard inhaalde?",
                    false
                    ));

                /// ------------------------------------ ///
                ///              scenario 2              ///
                /// ------------------------------------ ///

                eventFrames.Add(1712, new Event(
                    1712,
                    null,
                    Event.FrameEventType.StartScenario,
                    false,
                    false
                    ));

                eventFrames.Add(1930, new Event(
                    1930,
                    null,
                    Event.FrameEventType.MergeLeft,
                    true,
                    true,
                    "",
                    "Inhalen in een gevaarlijke situatie",
                    "Je nadert een gevaarlijke situatie waar je iemand in moet halen. Haal op tijd in, maar let goed op of dit kan\n\nKijk eerst in je binnenspiegel, daarna in je linkerbuitenspiegel en tot slot over je linkerschouder.",
                    false
                    ));

                eventFrames.Add(2043, new Event(
                    2043,
                    null,
                    Event.FrameEventType.MergeRight,
                    true,
                    true,
                    "",
                    "Inhalen in een gevaarlijke situatie",
                    "Voor het terugvoegen doe je hetzelfde, maar dan gespiegeld.\n\nKijk nu eerst weer in de binnenspiegel, vervolgens in de rechterbuitenspiegel en tot slot ook over de rechterschouder.",
                    false
                    ));

                eventFrames.Add(2100, new Event(
                    2100,
                    null,
                    Event.FrameEventType.EndScenario,
                    false,
                    false
                    ));

                /// ------------------------------------ ///
                ///              scenario 3              ///
                /// ------------------------------------ ///

                eventFrames.Add(2300, new Event(
                    2300,
                    null,
                    Event.FrameEventType.StartScenario,
                    false,
                    false
                    ));

                eventFrames.Add(2380, new Event(
                    2380,
                    new List<QuizAnswer>()
                    {
                new QuizAnswer("Ja", false),
                new QuizAnswer("Nee", true)
                    },
                    Event.FrameEventType.Quiz,
                    true,
                    true,
                    "",
                    "Quizvraag - Voorrang",
                    "Je nadert een eenvoudig kruispunt, waar we linksaf gaan slaan.\n\nHeb je in deze situatie voorrang van het verkeer van voren?",
                    false
                    ));

                eventFrames.Add(2645, new Event(
                    2645,
                    null,
                    Event.FrameEventType.MergeLeft,
                    true,
                    true,
                    "",
                    "Afslag en voorrang",
                    "Wanneer de weg vrij is kijk je weer in je binnenspiegel, in de linkerbuitenspiegel en tot slot ook over je linkerschouder, om te zien of het veilig is. Let hierbij ook op het kruisende fietspad.",
                    false
                    ));

                eventFrames.Add(2720, new Event(
                    2720,
                    null,
                    Event.FrameEventType.EndScenario,
                    false,
                    false
                    ));

                break;
        }*/

        return eventFrames;
    }
}
