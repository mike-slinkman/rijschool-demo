﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventToJsonDB : MonoBehaviour {

	// Use this for initialization
	void Start () {
        ScenarioDataBase dataBase = ScenarioDataBase.Instance;

        Scenario script10 = new Scenario(dataBase.GetUniqueID(), "Script10");
        script10.VideoName = "scannen";
        dataBase.AddScenario(script10);

        Scenario script25 = new Scenario(dataBase.GetUniqueID(), "Script25");
        script25.VideoName = "links_afslaan";
        dataBase.AddScenario(script25);

        Scenario script30 = new Scenario(dataBase.GetUniqueID(), "Script30");
        script30.VideoName = "rijstrook_wisselen";
        dataBase.AddScenario(script30);


        dataBase.Save();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
