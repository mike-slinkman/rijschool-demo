﻿using UnityEngine;
using DG.Tweening;

public class PopUpScript : MonoBehaviour {

    //does popup have image or button?
    public bool HasImage { get; set; }
    public bool HasButton { get; set; }

    private string goal = "";
    public string Goal { get; set; }

    private int currentStage = 0;

    public bool Completed { get; set; }
    public bool MarkedForDeletion { get; set; }

    private Event frameEvent;

    /// <summary>
    /// visualizes the content of the pop up and flies it in the screen
    /// </summary>
    /// <param name="the event frame"></param>
    public virtual void InitPopUp(Event frameEvent, PopUpElementsScript UIElements)
    {
        this.frameEvent = frameEvent;
        
        HasImage = !(frameEvent.ImageName == "");
        HasButton = false;
        if (frameEvent.EventType == Event.FrameEventType.ContinueButton || frameEvent.EventType == Event.FrameEventType.StillGame)
            HasButton = true;

        //set pop up text
        UIElements.HeaderText.text = frameEvent.Header;
        UIElements.MessageText.text = frameEvent.Message;

        //set image
        if (HasImage)
            UIElements.MessageImage.sprite = Resources.Load<Sprite>("PopUpSprites/" + frameEvent.ImageName);

        //disable buttons and set correct BG
        UIElements.Button.gameObject.SetActive(HasButton);
        UIElements.ButtonOverlay.gameObject.SetActive(HasButton);
        UIElements.ButtonText.gameObject.SetActive(HasButton);
        UIElements.LongBG.gameObject.SetActive(HasButton);
        UIElements.ShortBG.gameObject.SetActive(!HasButton);
        
        if (HasButton)
            UIElements.ButtonOverlay.GetComponent<FillButtonScript>().onfillCompleted += onButtonInteract;
        
        //SetGoal();
        VisualizePopUp();
    }

    public virtual void VisualizePopUp()
    {
        SFXManager.instance.PlaySFX("newPopUp");
        gameObject.SetActive(true);
        gameObject.transform.DOLocalMoveX(-10, 1f).From();
    }

    /// <summary>
    /// user has interacted with the continue button
    /// </summary>
    public void onButtonInteract(GameObject obj, string buttonName = "continue")
    {
        Debug.Log("user interacted with button: " + buttonName);
        SFXManager.instance.PlaySFX("completedPopUp");
        CompletedAndRemove();
    }

    public void CompletedAndRemove()
    {
        if (VideoPlayerScript.instance != null)
            VideoPlayerScript.instance.ContinueVideo(frameEvent);

        Completed = true;
        MarkedForDeletion = true;
    }

    /*private void SetGoal()
    {
        switch (frameEvent.EventType)
        {
            case Event.FrameEventType.LeftMirror:
                goal = "LinkerSpiegel";
                break;
            case Event.FrameEventType.RightMirror:
                goal = "RechterSpiegel";
                break;
            case Event.FrameEventType.LeftShoulder:
                goal = "LinkerSchouder";
                break;
            case Event.FrameEventType.RightShoulder:
                goal = "RechterSchouder";
                break;
            case Event.FrameEventType.MidMirror:
                goal = "BinnenSpiegel";
                break;
            case Event.FrameEventType.FarLeft:
                goal = "LinksBreed";
                break;
            case Event.FrameEventType.FarRight:
                goal = "RechtsBreed";
                break;
            case Event.FrameEventType.FarWide:
                goal = "LinksBreed";
                break;
            case Event.FrameEventType.MergeLeft:
                goal = "BinnenSpiegel";
                break;
            case Event.FrameEventType.MergeRight:
                goal = "BinnenSpiegel";
                break;
            case Event.FrameEventType.TurnLeft:
                goal = "BinnenSpiegel";
                break;
            case Event.FrameEventType.TurnRight:
                goal = "BinnenSpiegel";
                break;
            case Event.FrameEventType.ExitTurn:
                goal = "BinnenSpiegel";
                break;
            case Event.FrameEventType.NearRoundAbout:
                goal = "BinnenSpiegel";
                break;
            default:
                goal = "";
                break;
        }
    }*/

    /// <summary>
    /// Tell pop up that user has looked at overlay, check if this matches the goals
    /// </summary>
    /*public void CheckForGoals(string target)
    {
        Debug.Log("looked at: " + target + ", goal: " + goal);
        if (target == goal)
        {
            if (frameEvent.EventType == Event.FrameEventType.MergeRight)
            {
                currentStage++;
                if (currentStage == 1)
                    goal = "RechterSpiegel";
                else if (currentStage == 2)
                    goal = "RechterSchouder";
                else if (currentStage == 3)
                    CompletedAndRemove();
                else
                    Debug.LogWarning("ERROR: Stage of event unavailable. gr Boy");
            }
            else if (frameEvent.EventType == Event.FrameEventType.MergeLeft)
            {
                currentStage++;
                if (currentStage == 1)
                    goal = "LinkerSpiegel";
                else if (currentStage == 2)
                    goal = "LinkerSchouder";
                else if (currentStage == 3)
                    CompletedAndRemove();
                else
                    Debug.LogWarning("ERROR: Stage of event unavailable. gr Boy");
            }
            else if (frameEvent.EventType == Event.FrameEventType.FarWide)
            {
                currentStage++;
                if (currentStage == 1)
                    goal = "RechtsBreed";
                else if (currentStage == 2)
                    CompletedAndRemove();
                else
                    Debug.LogWarning("ERROR: Stage of event unavailable. gr Boy");
            }
            else if (frameEvent.EventType == Event.FrameEventType.TurnRight)
            {
                currentStage++;
                if (currentStage == 1)
                    goal = "Vooruit";
                else if (currentStage == 2)
                    goal = "RechterSpiegel";
                else if (currentStage == 3)
                    goal = "RechterSchouder";
                else if (currentStage == 4)
                    CompletedAndRemove();
                else
                    Debug.LogWarning("ERROR: Stage of event unavailable. gr Boy");
            }
            else if (frameEvent.EventType == Event.FrameEventType.TurnLeft)
            {
                currentStage++;
                if (currentStage == 1)
                    goal = "Vooruit";
                else if (currentStage == 2)
                    goal = "LinkerSpiegel";
                else if (currentStage == 3)
                    goal = "LinkerSchouder";
                else if (currentStage == 4)
                    CompletedAndRemove();
                else
                    Debug.LogWarning("ERROR: Stage of event unavailable. gr Boy");
            }
            else if (frameEvent.EventType == Event.FrameEventType.ExitTurn)
            {
                currentStage++;
                if (currentStage == 1)
                    goal = "LinkerSpiegel";
                else if (currentStage == 2)
                    CompletedAndRemove();
                else
                    Debug.LogWarning("ERROR: Stage of event unavailable. gr Boy");
            }
            else if (frameEvent.EventType == Event.FrameEventType.NearRoundAbout)
            {
                currentStage++;
                if (currentStage == 1)
                    goal = "Vooruit";
                else if (currentStage == 2)
                    goal = "LinkerSpiegel";
                else if (currentStage == 3)
                    goal = "RechterSpiegel";
                else if (currentStage == 4)
                    CompletedAndRemove();
                else
                    Debug.LogWarning("ERROR: Stage of event unavailable. gr Boy");
            }
            else
            {
                CompletedAndRemove();
            }

            SFXManager.instance.PlaySFX("completedPopUp");
        }
    }*/

}
