﻿using System.Collections.Generic;
using UnityEngine;

public class ScenarioContainer : MonoBehaviour {

    public void Awake()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }

    public Scenario ActiveScenario { get; set; }
    public List<Event> OldScoreList { get; set; }
    public List<Event> UpdatedDifficultyList { get; set; }
}
