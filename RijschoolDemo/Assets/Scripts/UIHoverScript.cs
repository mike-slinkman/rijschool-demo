﻿using UnityEngine;
using System;

public class UIHoverScript : MonoBehaviour {

    float originalX;
    float originalY;
    float originalZ;

    public float floatStrengthX; 
    public float floatStrengthY; 
    public float floatStrengthZ; 

    void Start()
    {
        this.originalX = this.transform.position.x;
        this.originalY = this.transform.position.y;
        this.originalZ = this.transform.position.z;

        floatStrengthX = (0.005f + UnityEngine.Random.Range(-0.003f, 0.003f));
        floatStrengthY = (0.005f + UnityEngine.Random.Range(-0.003f, 0.003f));
        floatStrengthZ = (0.005f + UnityEngine.Random.Range(-0.003f, 0.003f));
}

    void Update()
    {
        transform.position = new Vector3(originalX + ((float)Math.Sin(Time.time) * floatStrengthX),
            originalY + ((float)Math.Sin(Time.time) * floatStrengthY),
            transform.position.z);
    }
}
