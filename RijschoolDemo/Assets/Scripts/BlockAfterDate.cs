﻿using System;
using System.IO;
using UnityEngine;

public class BlockAfterDate : MonoBehaviour {
    DateTime maxdate;

    string videopath;
    string jsonpath;

	// Use this for initialization
	void Start () {
        DeleteEverything();
        
        maxdate = new DateTime(2030, 6, 17); //Enter first day that the app should be disabled
        if (DateTime.Now > maxdate)
        {
            Application.Quit();
        }
        else
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("DynamicMenu");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void DeleteEverything()
    {
        videopath = Path.Combine(Application.persistentDataPath, "Videos");
        jsonpath = Path.Combine(Application.persistentDataPath, "Databases");
        System.IO.DirectoryInfo di;
        if (Directory.Exists(videopath))
        {
            di = new DirectoryInfo(videopath);


            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
        }
        if (Directory.Exists(jsonpath))
        { 

            di = new DirectoryInfo(jsonpath);

            foreach (FileInfo file in di.GetFiles())
            {
                if (file.Name != "SaveGame.json")
                    file.Delete();
            }
        }
    }
}
