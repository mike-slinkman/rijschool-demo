﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StillGameManager : MonoBehaviour {

    [SerializeField]
    private List<FillButtonScript> buttons = new List<FillButtonScript>();

    [SerializeField]
    FadeScript fader;

    string goal = "";
    [SerializeField]
    List<string> goals;

    Event currentEvent = null;

    // Use this for initialization
    void Start () {
        goals = new List<string>() { "richtingaanwijzerhandel", "ruitenwisserhandel", "snelheidsmeter", "versnellingspook", "oliepeilstok", "ruitensproeierreservoir", "koelvloeistofreservoir" };

        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        Debug.Log("added popup from start");
        StartNewPopUp();
        AddButtons();
	}

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if (arg0.buildIndex == 0)
        {
            SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
            Destroy(this);
        } else
        {
            foreach (StillGameManager copy in GameObject.FindObjectsOfType<StillGameManager>())
                if (copy != this) Destroy(copy);

            Debug.Log("added popup from scene loaded");
            StartNewPopUp(goal);

            AddButtons();
        }
    }

    void AddButtons()
    {
        buttons.Clear();
        Debug.Log("adding buttons");
        buttons = GameObject.FindObjectsOfType<FillButtonScript>().ToList();

        foreach (FillButtonScript btn in buttons)
            btn.onfillCompleted += OnButtonSelected;
    }

    void Update()
    {
        if (currentEvent != null)
        {
            if (currentEvent.MarkedForDeletion)
            {
                currentEvent = null;
            }
        }
    }

    void OnButtonSelected (GameObject obj, string objName) {
        
        if (objName == "Buiten" || objName == "Binnen")
        {
            string sceneToLoad = (objName == "Buiten") ? "Buiten" : "Auto";
            SceneManager.LoadSceneAsync(sceneToLoad);
            return;
        }

        if (goal != "")
        {
            StartCoroutine(DisplayAnswer(obj, objName == goal));
        }                      
	}

    IEnumerator DisplayAnswer(GameObject obj, bool isCorrect) {
        //remove old pop up
        currentEvent.Done(false);
        foreach (AbstractFillButton b in buttons)
        {
            b.Locked = true;
        }

        //make sfx
        string sfx = (isCorrect) ? "completedPopUp" : "wrongAnswer";
        SFXManager.instance.PlaySFX(sfx);

        //swap sprite
        Sprite oldSprite = obj.GetComponent<Image>().sprite;
        string newSprite = (isCorrect) ? "ButtonOverlayCorrect" : "ButtonOverlayWrong";
        obj.GetComponent<Image>().sprite = Resources.Load<Sprite>("PopUpSprites/" + newSprite);

        //wait
        yield return new WaitForSeconds(2f);

        //swap sprite back
        obj.GetComponent<Image>().sprite = oldSprite;
        foreach (AbstractFillButton b in buttons)
        {
            b.Locked = false;
        }
        //new pop up
        if (goals.Count > 0)
        {
            Debug.Log("added popup from completion");
            StartNewPopUp();
        } else
        {
            SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
            SceneManager.LoadSceneAsync("Menu");
        } 

    }

    void StartNewPopUp(string oldGoal = null)
    {
        if (oldGoal == null)
        {
            goal = goals[Random.Range(0, goals.Count - 1)];
            goals.Remove(goal);
        }

        GameObject popUpObject = (GameObject)Instantiate(Resources.Load("PopUpHolderImage"), GameObject.FindGameObjectWithTag("PopUpHolder").transform, false);

        //add a script and init pop up, for later 
        PopUpElementsScript UI = popUpObject.GetComponent<PopUpElementsScript>();
        Event _event = new Event(
                 0, 0, null, Event.FrameEventType.StillGame, false, true, "ob09-600x300", "AUTO-ONDERDELEN", "Waar bevindt de " + goal + " zich?", false
             );

        currentEvent = _event;
        currentEvent.Activate();
    }
}
