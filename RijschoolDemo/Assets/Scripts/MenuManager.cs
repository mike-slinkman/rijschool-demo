﻿/// MenuManager, responsible for all menu related tasks. this includes building, keeping track of, scrolling trough and navigating the menu.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MenuManager : MonoBehaviour {
    
    [SerializeField]
    GameObject backButton;
    [SerializeField]
    GameObject menuPrefab;
    [SerializeField]
    GameObject menuContainer;

    RectTransform menuContainerRect;
    [SerializeField]
    GameObject LeftArrowImage;
    [SerializeField]
    GameObject RightArrowImage;

    bool canScrollLeft = false;
    bool canScrollRight = false;

    int ClosestItem = -1;
    float viewport = 750;

    List<RijschoolMenuItem> currentmenu = new List<RijschoolMenuItem>();
    RijschoolMenuItem currentroot = null;
    List<float> previousLayerXposition = new List<float>();

	// Use this for initialization
	void Start ()
    {
        menuContainerRect = menuContainer.GetComponent<RectTransform>();
	}

    /// <summary>
    /// used to activate/deactivate the scroll buttons
    /// </summary>
    private void Update()
    {
        if (menuContainerRect.transform.localPosition.x > -1)
            canScrollLeft = false;
        else
            canScrollLeft = true;

        if (Mathf.Abs(menuContainerRect.transform.localPosition.x) > menuContainerRect.sizeDelta.x - 1)
            canScrollRight = false;
        else
            canScrollRight = true;

        LeftArrowImage.SetActive(canScrollLeft);
        RightArrowImage.SetActive(canScrollRight);
    }

    /// <summary>
    /// creates a menu based on the children of the passed menu item
    /// </summary>
    /// <param name="buildRoot">root for the menu, children will be shown</param>
    public void BuildMenu(RijschoolMenuItem buildRoot)
    {
        ClearMenu();
        previousLayerXposition.Add(menuContainerRect.localPosition.x);
        currentroot = buildRoot;
        
        backButton.SetActive(!buildRoot.IsRoot);

        currentmenu = MenuDataBase.Instance.GetMenuItemsByIDs(buildRoot.children);
        List<RijschoolMenuItem> removefromcurrentmenu = new List<RijschoolMenuItem>();
        foreach (RijschoolMenuItem menuitem in currentmenu)
        {
            if (menuitem.selectAction != RijschoolMenuItem.SelectAction.MenuLayer || (menuitem.selectAction == RijschoolMenuItem.SelectAction.MenuLayer && menuitem.HasChildren()))
            {
                //create items if they arent a menulayer without children
                GameObject newobject = Instantiate<GameObject>(menuPrefab);
                newobject.transform.SetParent(menuContainer.transform, false);
                newobject.transform.GetChild(0).Find("Header").gameObject.GetComponent<Text>().text = menuitem.Name;
                newobject.transform.GetChild(0).Find("Description").gameObject.GetComponent<Text>().text = menuitem.Description;
                newobject.transform.GetChild(0).Find("Image").gameObject.GetComponent<Image>().sprite = menuitem.IconSprite;

                newobject.transform.GetChild(0).Find("Progress").gameObject.GetComponent<Text>().text = string.Format("{0:0.00}", SaveGameDataBase.Instance.GetSkillAverageForScenario(menuitem.ScenarioID)) + "%";
                newobject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            }
            else
            {
                //add items that are childless menulayers to the remove list
                removefromcurrentmenu.Add(menuitem);
            }
        }
        foreach(RijschoolMenuItem removeitem in removefromcurrentmenu)
        {
            currentmenu.Remove(removeitem);
        }
        ClosestItem = 0;
        CenterOnNearestItem();
    }

    /// <summary>
    /// Builds the menu one folder higher than we currently are. 
    /// </summary>
    public void OnBack()
    {
        BuildMenu(MenuDataBase.Instance.GetMenuItemByID(currentroot.parentID));
        //buildmenu ads the current menu layer to the previousLayerXPosition list. we need to use the count -2 to get to the layer that was active before this one that was called back on.
        Debug.Log("went back, setting position to item: " + previousLayerXposition[previousLayerXposition.Count - 2]);
        SetMenuPosition(previousLayerXposition[previousLayerXposition.Count - 2]);
        previousLayerXposition.RemoveAt(previousLayerXposition.Count - 1);
        previousLayerXposition.RemoveAt(previousLayerXposition.Count - 1);
    }

    /// <summary>
    /// Calls OnSelect on the currently centered menuitem.
    /// </summary>
    public void OnSelect()
    {
        currentmenu[ClosestItem-1].OnSelect(this);
    }

    /// <summary>
    /// Deletes all items currently in the menu.
    /// </summary>
    void ClearMenu()
    {
        for (int i = menuContainer.transform.childCount - 1; i >= 0; i--)
        {
            Transform child = menuContainer.transform.GetChild(i);
            Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Scrolls the menu towards the left.
    /// </summary>
    /// <param name="distance">Distance to scroll</param>
    public void ScrollLeft(float distance)
    {
        menuContainerRect.position += new Vector3(distance, 0, 0);
    }

    /// <summary>
    /// Scrolls the menu towards the right.
    /// </summary>
    /// <param name="distance">Distance to scroll</param>
    public void ScrollRight(float distance)
    {
        menuContainerRect.position -= new Vector3(distance, 0, 0);
    }

    /// <summary>
    /// Centers the menu on the nearest item.
    /// </summary>
    public void CenterOnNearestItem()
    { 
        float center = menuContainerRect.localPosition.x - (viewport / 2);
        float closestdistance = float.PositiveInfinity;
        for (int i = menuContainer.transform.childCount; i > 0; i--)
        {
            float currentdistance = Mathf.Abs(menuContainer.transform.GetChild(i - 1).localPosition.x - Mathf.Abs(center));
            if (currentdistance < closestdistance)
            {
                closestdistance = currentdistance;
                ClosestItem = i;
            }
        }
        CenterOnMenuItem(ClosestItem);
    }

    /// <summary>
    /// centers the menu on the menu item based on its place in the menu where the most left item is item 1.
    /// </summary>
    /// <param name="itemToCenter">item to center on based on number from left to right</param>
    void CenterOnMenuItem(int itemToCenter)
    {
        float newx = -(menuContainer.transform.GetChild(itemToCenter - 1).transform.localPosition.x - (viewport/2));
        Debug.Log("setting scrollview to x: " + newx);
        menuContainerRect.DOLocalMove(new Vector3(newx, menuContainerRect.localPosition.y, menuContainerRect.localPosition.z), 0.5f);
    }

    /// <summary>
    /// Scrolls the menu to to the provided x position.
    /// </summary>
    /// <param name="xPos">New menu scrollPosition</param>
    void SetMenuPosition(float xPos)
    {
        menuContainerRect.DOLocalMove(new Vector3(xPos, menuContainerRect.localPosition.y, menuContainerRect.localPosition.z), 0.5f);
    }
}
