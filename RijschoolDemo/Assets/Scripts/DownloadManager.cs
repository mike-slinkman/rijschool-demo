﻿using System.IO;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;

public class DownloadManager : MonoBehaviour{

    public enum DownloadType
    {
        Menu,
        Video,
        Events,
        Positions,
        Scenarios,
        Image
    }

    UnityWebRequest www;
    public bool IsDownloading;
    string baseurl = "https://www.devcake.nl/Smit/";

    string downloadName;
    DownloadType downloadType;
    Action DownloadDone;
    //progressDelegate?
    //downloadDelegate?
    public static DownloadManager instance;

    [SerializeField]
    Text uitextfield;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    /// <summary>
    /// Download a file from the server.
    /// </summary>
    /// <param name="oDownloadType">Type of file to download.</param>
    /// <param name="oFileName">Name of file on server.</param>
    /// <param name="oAction">Function to call when download is done.</param>
    public void Download(DownloadType oDownloadType, string oFileName, Action oAction)
    {
        //Debug.Log("Download called, check if file exists next");
        downloadType = oDownloadType;
        downloadName = oFileName;
        DownloadDone = oAction;
        StartCoroutine(CheckDownload());
    }

    void Update()
    {
        //make this return info to specified delegate
        if (IsDownloading)
        {
            if (uitextfield != null)
                uitextfield.text = "bezig met downloaden: " + (int)(www.downloadProgress * 100) + "%";
            else
                Debug.Log("progress: " + (int)(www.downloadProgress * 100));
        }
        else
        {
            if (uitextfield != null)
                uitextfield.text = "Klaar voor gebruik.";
        }
        
    }

    IEnumerator CheckDownload()
    {

        if (File.Exists(GetPath()))
        {
            //Debug.Log("already downloaded, done!");
            //Do nothing
            DownloadDone();
        }
        else
        {
            if (!Directory.Exists(GetPath(false)))
            {
                Directory.CreateDirectory(GetPath(false));
            }
            //Debug.Log("file missing, check connection state");
            StartCoroutine(CheckConnectionState());
        }
        yield return new WaitForEndOfFrame();
    }

    IEnumerator CheckConnectionState()
    {
        switch (Application.internetReachability)
        {
            case NetworkReachability.ReachableViaLocalAreaNetwork:
                //Debug.Log("wifi/lan, start downloading.");
                StartCoroutine(DownloadFile());
                break;
            case NetworkReachability.ReachableViaCarrierDataNetwork:
                //UIText.text = "Maak verbinding met WiFi, start vervolgens opnieuw op";
                //Debug.Log("Carrier data, please get wifi");
                break;
            case NetworkReachability.NotReachable:
                //UIText.text = "Maak verbinding met WiFi, start vervolgens opnieuw op";
                //Debug.Log("no connection");
                break;
        }
        yield return new WaitForEndOfFrame();
    }

    IEnumerator DownloadFile()
    {
        IsDownloading = true;
        //Debug.Log("build download url.");
        string url = baseurl;
        switch (downloadType)
        {
            case DownloadType.Video:
                url += "videos/";
                url += downloadName;
                url += ".mp4";
                break;
            case DownloadType.Menu:
            case DownloadType.Events:
            case DownloadType.Positions:
            case DownloadType.Scenarios:
                url += "jsondb/";
                url += downloadName;
                url += ".json";
                break;
            case DownloadType.Image:
                url += "images/";
                url += downloadName;
                url += ".png";
                break;
            default:
                break;
        }
        Debug.Log("Downloading from url: " + url);
        www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            byte[] bytes = www.downloadHandler.data;
            string savelocation = GetPath();
            if (!Directory.Exists(GetPath(false)))
                Directory.CreateDirectory(GetPath(false));

            File.WriteAllBytes(savelocation, bytes);
        }

        IsDownloading = false;
        DownloadDone();
    }

    /// <summary>
    /// Gets file path. gets directory if parameter is false
    /// </summary>
    /// <param name="file">default gets file location. false gets directory</param>
    /// <returns>path as string</returns>
    private string GetPath(bool file = true)
    {
        string path = "";
        switch (downloadType)
        {
            case DownloadType.Video:
                path = Path.Combine(Application.persistentDataPath, "Videos");
                if (file)
                    path = Path.Combine(path, downloadName + ".mp4");
                break;
            case DownloadType.Menu:
            case DownloadType.Events:
            case DownloadType.Positions:
            case DownloadType.Scenarios:
                path = Path.Combine(Application.persistentDataPath, "Databases");
                if (file)
                    path = Path.Combine(path, downloadName + ".json");
                break;
            case DownloadType.Image:
                path = Path.Combine(Application.persistentDataPath, "images");
                if (file)
                    path = Path.Combine(path, downloadName + ".png");
                break;
            default:
                break;
        }
        return path;
    }
}
