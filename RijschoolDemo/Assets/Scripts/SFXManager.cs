﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SFXManager : MonoBehaviour {

    //singleton var
    public static SFXManager instance = null;

    //sfx proporty
    [SerializeField]
    private AudioClip completedPopUp;
    [SerializeField]
    private AudioClip newPopUp;
    [SerializeField]
    private AudioClip wrongAnswer;
    [SerializeField]
    private AudioClip hover;

    private AudioSource source;

    // Use this for initialization
    void Awake () {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        source = this.GetComponent<AudioSource>();
    }

    /// <summary>
    /// plays the audio that is passed as a parameter
    /// </summary>
    /// <param name="sfx"></param>
    public void PlaySFX(string sfx)
    {
        SelectAudio(sfx);

        if (source.clip != null)
            source.Play();
    }

    private void SelectAudio(string sfxname)
    {
        switch (sfxname)
        {
            case "completedPopUp":
                source.clip = completedPopUp;
                break;
            case "newPopUp":
                source.clip = newPopUp;
                break;
            case "wrongAnswer":
                source.clip = wrongAnswer;
                break;
            case "hover":
                source.clip = hover;
                break;
            default:
                Debug.LogWarning("ERROR: unknown SFX name. gr Boy");
                break;

        }
    }
	
}
