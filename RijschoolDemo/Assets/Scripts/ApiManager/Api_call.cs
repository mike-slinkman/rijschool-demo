﻿using System.Collections;
using UnityEngine;

interface Api_call
{
    IEnumerator Call(WWWForm form);
}