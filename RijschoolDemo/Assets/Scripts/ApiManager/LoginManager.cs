﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class LoginManager : MonoBehaviour
{
    #region Controls
    public Button submit;
    public InputField Username, Password;
    public Toggle Remember;
    public Text Error;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        submit.onClick.AddListener(sendLoginPost);
    }

    void sendLoginPost()
    {
        //Create postData usng WWWForm to convert form data to json
        WWWForm form = new WWWForm();
        form.AddField("email", "" + Username.text);
        form.AddField("password", "" + Password.text);
        form.AddField("remember_me", Convert.ToInt32(Remember.isOn));

        //StartCoroutine for sending the post request

        Api_call login = new Login(ApiManager.Base_Url + "/api/auth/login", this);
        StartCoroutine(login.Call(form));
    }

    public void ShowError(string errorText, Color clr)
    {
        Error.text = errorText;
        Error.color = clr;
    }
}
