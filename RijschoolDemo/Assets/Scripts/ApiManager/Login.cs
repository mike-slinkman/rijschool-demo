﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

class Login : Api_call
{
    readonly string url;
    LoginManager lm;
    public Login(string newurl, Object obj)
    {
        lm = (LoginManager)obj;
        url = newurl;
    }

    public IEnumerator Call(WWWForm form)
    {
        Debug.Log("Logging in.");
        //Send form data as post to api at url
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            Debug.Log("Sending request");
            //Send request
            yield return www.SendWebRequest();

            switch (www.responseCode)
            {
                //Response code 200 == User was authorized
                case 200:
                    UserAccess UA = JsonUtility.FromJson<UserAccess>(www.downloadHandler.text);
                    lm.ShowError("User was authorized!", Color.green);
                    //Open next scene here!
                    SceneManager.LoadScene("DynamicMenu");
                    break;
                //Response code 401 == User wat not authorized
                case 401:
                    lm.ShowError("User was not authorized!", Color.red);
                    break;
                //Response code 422 == Incorrect data and/or datatypes
                case 422:
                    Error err = JsonUtility.FromJson<Error>(www.downloadHandler.text);
                    lm.ShowError(err.ToString(), Color.red);
                    break;
                //Any other unknown errors
                default:
                    lm.ShowError("Unknown error " + www.responseCode, Color.red);
                    break;
            }
        }
    }

    public class UserAccess
    {
        public string access_token;
        public string token_type;
        public string expires_at;
    }

    public class Error
    {
        public string[] email = new string[1];
        public string[] password = new string[1];
        public string[] remeber_me = new string[1];

        public Error()
        {
            email[0] = "";
            password[0] = "";
            remeber_me[0] = "";
        }

        public override string ToString()
        {
            string output = "";

            output += email[0];

            if (output != "")
            {
                output += " ";
            }

            output += password[0];

            if (output != "")
            {
                output += " ";
            }

            output += remeber_me[0];

            return output;
        }
    }

}