﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginScript : MonoBehaviour
{
    public const string serverApi_BaseUrl = "http://127.0.0.1:8000";

    #region Controls
    public Button submit;
    public InputField Username, Password;
    public Toggle Remember;
    public Text Error;
    #endregion

    void Start()
    {
        submit.onClick.AddListener(sendLoginPost);
    }

    void sendLoginPost()
    {
        //Create postData usng WWWForm to convert form data to json
        WWWForm form = new WWWForm();
        form.AddField("email", "" + Username.text);
        form.AddField("password", "" + Password.text);
        form.AddField("remember_me", Convert.ToInt32(Remember.isOn));

        //StartCoroutine for sending the post request
        StartCoroutine(LoginPost(serverApi_BaseUrl + "/api/auth/login", form));
        Debug.Log("Logging in");
    }

    IEnumerator LoginPost(string url, WWWForm form)
    {
        //Send form data as post to api at url
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            //Send request
            yield return www.SendWebRequest();

            //Check if there is a NetworkError or HttpError
            if (www.isNetworkError || www.isHttpError)
            {
                //If there is a NetworkError or a HttpError send the user a visual error
                ShowError("User was not authorized!");
            }
            else //Else if user is authorized or the API return code 200 because it automatically redirects the user after receiving incorrect json data
            {
                //Trying to turn the body into an UserAccess object
                //If it fails the user was not authorized
                //Should only trigger when the json is invalid. Example 1: Empty E-mail and/or password
                try
                {
                    //Save access_token information somewhere
                    UserAccess UA = JsonUtility.FromJson<UserAccess>(www.downloadHandler.text);
                    ShowError("User was authorized!" + www.error);
                    SceneManager.LoadScene("DynamicMenu", LoadSceneMode.Single);
                } catch
                {
                    //User was not authorized
                    ShowError("User was not authorized! 200 " + www.error);
                }
            }
        }
    }

    public void ShowError(string errorText)
    {
        Error.text = errorText;
    }

    public class UserAccess
    {
        public string access_token;
        public string token_type;
        public string expires_at;
    } 
}

