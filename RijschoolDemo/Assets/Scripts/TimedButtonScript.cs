﻿using UnityEngine;

public class TimedButtonScript : MonoBehaviour {

    // filling vars
    private bool startTimer;
    private float timer = 0f;

    [SerializeField]
    private float timeToComplete = 0.3f;
    
    public delegate void OnTimerCompleted(string overlayName);
    public OnTimerCompleted onTimerCompleted;

    private bool buttonLocked;
    
    // Update is called once per frame
    void Update()
    {
        if (startTimer)
        {
            timer += Time.deltaTime;
            if (timer >= timeToComplete && !buttonLocked)
            {
                if (onTimerCompleted != null)
                {
                    buttonLocked = true;
                    onTimerCompleted(this.gameObject.name);
                }
            }
        }
    }

    // Update is called once per frame
    public void OnGazeOver()
    {
        startTimer = true;
    }

    public void OnGazeOut()
    {
        buttonLocked = false;
        startTimer = false;
        timer = 0;
    }
}
