﻿using UnityEngine;

public class EditorDataManager {

    [System.NonSerialized]
    private static EditorDataManager _instance;
    public static EditorDataManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new EditorDataManager();
                Debug.Log("_instance created");
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    public delegate void OnDownloadsDone(bool value);
    public OnDownloadsDone onDownloadsDone;
    private bool _downloadsDone = false;
    public bool DownloadsDone
    {
        get
        {
            return _downloadsDone;
        }
        set
        {
            _downloadsDone = value;
            if (onDownloadsDone != null)
                onDownloadsDone(_downloadsDone);
        }
    }

    public delegate void OnEditScenarioChange(Scenario scenario);
    public OnEditScenarioChange onEditScenarioChange;
    private Scenario _editScenario;
    public Scenario EditScenario
    {
        get
        {
            return _editScenario;
        }
        set
        {
            _editScenario = value;
            if (onEditScenarioChange != null)
                onEditScenarioChange(_editScenario);
        }
    }

    public delegate void OnEditEventChange(Event eEvent);
    public OnEditEventChange onEditEventChange;
    private Event _editEvent;
    public Event EditEvent
    {
        get
        {
            return _editEvent;
        }
        set
        {
            _editEvent = value;
            if (onEditEventChange != null)
                onEditEventChange(_editEvent);
        }
    }

    private int _currentFrame;
    public int CurrentFrame
    {
        get
        {
            return _currentFrame;
        }
        set
        {
            _currentFrame = value;
        }
    }
}
