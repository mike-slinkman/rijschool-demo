﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PerformanceChecker : MonoBehaviour {

    List<int> fpslist = new List<int>();
    bool recording = false;
	// Use this for initialization
	void Start () {
        SceneManager.sceneLoaded += OnSceneLoaded;
	}
	
	// Update is called once per frame
	void Update () {
        if (recording)
            fpslist.Add((int)(1 / Time.deltaTime));
	}

    public void StartRecording()
    {
        recording = true;
    }

    public void StopRecording()
    {
        recording = false;
    }

    public int GetPointOnePercentLow()
    {
        fpslist.Sort();
        int sum = 0;
        int pointonepercent = fpslist.Count / 1000;
        for (int i = 0; i < pointonepercent; i++)
            sum += fpslist[i];
        return sum / pointonepercent;
    }

    public int GetOnePercentLow()
    { 
        fpslist.Sort();
        int sum = 0;
        int onepercent = fpslist.Count / 100;
        for (int i = 0; i < onepercent; i++)
            sum += fpslist[i];
        return sum / onepercent;
    }

    public int GetAverage()
    {
        int sum = 0;
        foreach (int f in fpslist)
        {
            sum += f;
        }
        return sum / fpslist.Count;
    }

    public int GetOnePercentHigh()
    {
        fpslist.Sort();
        int sum = 0;
        int onepercent = fpslist.Count / 100;
        for (int i = fpslist.Count - onepercent; i < fpslist.Count; i++)
            sum += fpslist[i];
        return sum / onepercent;
    }

    public int GetPointOnePercentHigh()
    {
        fpslist.Sort();
        int sum = 0;
        int pointonepercent = fpslist.Count / 1000;
        for (int i = fpslist.Count - pointonepercent; i < fpslist.Count; i++)
            sum += fpslist[i];
        return sum / pointonepercent;
    }

    public void ResetStats()
    {
        recording = false;
        fpslist = new List<int>();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Menu")
            ResetStats();
    }
}
