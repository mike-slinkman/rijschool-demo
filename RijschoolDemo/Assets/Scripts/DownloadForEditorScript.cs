﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownloadForEditorScript : MonoBehaviour{

    List<DownloadStruct> downloadqueue;
    bool downloading;
    
    /// <summary>
    /// Downloads everything
    /// </summary>
    public void DownloadEverything()
    {
        //Debug.Log("Download started!");
        downloadqueue = new List<DownloadStruct>();
        StartCoroutine(DownloadEverythingEnumerator());
    }

    IEnumerator DownloadEverythingEnumerator()
    {
        //Debug.Log("itsdownloading!!!");
        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Scenarios, "ScenarioDB", DownloadDone);
        yield return new WaitUntil(() => !downloading);
        Debug.Log("scenarios done");

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Scenarios, "OverlayDB", DownloadDone);
        yield return new WaitUntil(() => !downloading);
        Debug.Log("overlays done");

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Scenarios, "Presets", DownloadDone);
        yield return new WaitUntil(() => !downloading);
        Debug.Log("presets done");

        ScenarioDataBase.Instance.ReloadData();
        ScenarioDataBase scenarioDataBase = ScenarioDataBase.Instance;
        Debug.Log("scenarios to go trough: " + scenarioDataBase.GetScenarios().Count);
        foreach (Scenario s in scenarioDataBase.GetScenarios())
        {
            Debug.Log("prepping " + s.Name + " for download");
            DownloadStruct downloadStruct = new DownloadStruct();
            downloadStruct.downloadType = DownloadManager.DownloadType.Events;
            downloadStruct.name = s.EventDBID;
            downloadqueue.Add(downloadStruct);

            DownloadStruct downloadStruct2 = new DownloadStruct();
            downloadStruct2.downloadType = DownloadManager.DownloadType.Video;
            downloadStruct2.name = s.VideoName;
            downloadqueue.Add(downloadStruct2);

            DownloadStruct downloadStruct3 = new DownloadStruct();
            downloadStruct3.downloadType = DownloadManager.DownloadType.Positions;
            downloadStruct3.name = s.PositionDBID;
            downloadqueue.Add(downloadStruct3);
        }

        for (int i = downloadqueue.Count; i > 0; i--)
        {
            downloading = true;
            DownloadManager.instance.Download(downloadqueue[i-1].downloadType, downloadqueue[i-1].name, DownloadDone);
            yield return new WaitUntil(() => !downloading);
            Debug.Log("Downloads left: " + i);
        }
        Debug.Log("Downloads done!");
        yield return new WaitForSeconds(1);
        EditorDataManager.Instance.DownloadsDone = true;
        yield return null;
    }

    void DownloadDone()
    {
        downloading = false;
    }
}

public struct DownloadStruct
{
    public DownloadManager.DownloadType downloadType;
    public string name;
}
