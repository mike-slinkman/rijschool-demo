﻿using System.Collections.Generic;

public class PositionDataDataBase : AbstractDataBase {

    private List<PositionData> positionDataList;

    /// <summary>
    /// makes a new database with position data.
    /// </summary>
    /// <param name="oFilename">Filename for database saves.</param>
    public PositionDataDataBase(string oFilename)
    {
        fileName = oFilename;
        positionDataList = LoadData<PositionData>();
        if (positionDataList == null)
            positionDataList = new List<PositionData>();
    }

    /// <summary>
    /// Returns all positiondatas.
    /// </summary>
    /// <returns>List of positiondata.</returns>
    public List<PositionData> GetPositionDatas()
    {
        return new List<PositionData>(positionDataList);
    }

    /// <summary>
    /// returns positiondata for a specific overlay.
    /// </summary>
    /// <param name="oOverlayID">ID of the overlay</param>
    /// <returns></returns>
    public PositionData GetPositionDataByOverlayID(int oOverlayID)
    {
        return positionDataList.Find(X => X.OverlayID == oOverlayID);
    }

    /// <summary>
    /// Adds the specified positiondata to the database.
    /// </summary>
    /// <param name="oPositionData">the positiondata to add.</param>
    public void AddPositionData(PositionData oPositionData)
    {
        positionDataList.Add(oPositionData);
    }

    /// <summary>
    /// Updates the positiondata for specified object.
    /// </summary>
    /// <param name="oPositionData">Object with old id and new info.</param>
    public void UpdatePositionData(PositionData oPositionData)
    {
        int i = positionDataList.FindIndex(X => X.OverlayID == oPositionData.OverlayID);
        positionDataList[i].RectDimentions = oPositionData.RectDimentions;
        positionDataList[i].RectTransform = oPositionData.RectTransform;
        positionDataList[i].Rotation = oPositionData.Rotation;
        positionDataList[i].Scale = oPositionData.Scale;
    }

    /// <summary>
    /// Deletes the specified positiondata.
    /// </summary>
    /// <param name="oPositionData">Positiondata to delete.</param>
    public void DeletePositionData(PositionData oPositionData)
    {
        positionDataList.Remove(oPositionData);
    }

    /// <summary>
    /// Saves the database to file.
    /// </summary>
    public void Save()
    {
        SaveData<PositionData>(positionDataList);
    }

}
