﻿using System.Collections.Generic;
using System;

[Serializable]
public class ScenarioDataBase : AbstractDataBase {

    private static ScenarioDataBase _instance;
    public static ScenarioDataBase Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ScenarioDataBase();
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    private List<Scenario> scenarioList;

    /// <summary>
    /// reloads the data in the Database and returns it.
    /// </summary>
    /// <returns>reloaded database</returns>
    public List<Scenario> ReloadData()
    {
        scenarioList = LoadData<Scenario>();
        return new List<Scenario>(scenarioList);
    }

    private ScenarioDataBase()
    {
        fileName = "ScenarioDB";
        scenarioList = LoadData<Scenario>();
        if (scenarioList == null)
            scenarioList = new List<Scenario>();
    }

    /// <summary>
    /// Returns all scenarios.
    /// </summary>
    /// <returns>list of scenarios.</returns>
    public List<Scenario> GetScenarios()
    {
        return new List<Scenario>(scenarioList);
    }

    /// <summary>
    /// Finds specific scenario based on specified ID.
    /// </summary>
    /// <param name="oID">ID of required Scenario.</param>
    /// <returns>Scenario with specified ID.</returns>
    public Scenario GetScenarioByID(int oID)
    {
        return scenarioList.Find(X => oID == X.ID);
    }

    /// <summary>
    /// Adds the specified scenario to the database. Call Save() to make permanent.
    /// </summary>
    /// <param name="oScenario">Scenario to add.</param>
    public void AddScenario(Scenario oScenario)
    {
        scenarioList.Add(oScenario);
    }

    /// <summary>
    /// Updates the scenario with new information. Call Save() to make permanent.
    /// </summary>
    /// <param name="oScenario">Updated version of scenario.</param>
    public void UpdateScenario(Scenario oScenario)
    {
        int i = scenarioList.FindIndex(X => oScenario.ID == X.ID);
        scenarioList[i].VideoName = oScenario.VideoName;
        scenarioList[i].IsDownloaded = oScenario.IsDownloaded;
    }

    /// <summary>
    /// Deletes specified scenario from database. Call Save() to make permanent.
    /// </summary>
    /// <param name="oScenario">Scenario to delete.</param>
    public void DeleteScenario(Scenario oScenario)
    {
        oScenario.LoadEventDB();
        oScenario.EventDB.DevDeleteDataBase(true);
        scenarioList.Remove(oScenario);
        Save();
    }

    /// <summary>
    /// Saves the database to file.
    /// </summary>
    public void Save()
    {
        SaveData<Scenario>(scenarioList);
    }

    /// <summary>
    /// returns the next unused ID.
    /// </summary>
    /// <returns>Unused ID.</returns>
    public int GetUniqueID()
    {
        int high = 0;
        foreach (Scenario s in scenarioList)
        {
            if (s.ID > high) high = s.ID;
        }
        return high + 1;
    }
}

