﻿using System.Collections.Generic;
using System;

[Serializable]
public class OverlayDataBase : AbstractDataBase {

    private static OverlayDataBase _instance;
    public static OverlayDataBase Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new OverlayDataBase();
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    private List<Overlay> overlayList;

    private OverlayDataBase()
    {
        fileName = "OverlayDB";
        overlayList = LoadData<Overlay>();
        if (overlayList == null)
            overlayList = new List<Overlay>();
    }

    /// <summary>
    /// Gets overlay by specified ID
    /// </summary>
    /// <param name="oID">ID of required overlay.</param>
    /// <returns>Overlay with ID.</returns>
	public Overlay GetOverlayByID(int oID)
    {
        return overlayList.Find(X => X.ID == oID);
    }

    /// <summary>
    /// Returns all overlays.
    /// </summary>
    /// <returns>List of Overlays.</returns>
    public List<Overlay> GetOverlays()
    {
        return new List<Overlay>(overlayList);
    }
    
    /// <summary>
    /// Combines positions with their respective overlays and returns them as a list. Use with position data from a scenario.
    /// </summary>
    /// <param name="oPositions">Positiondata to combine with overlays.</param>
    /// <returns>a list of overlays with the included position data.</returns>
    public List<Overlay> GetOverlayListForPositions(List<PositionData> oPositions)
    {
        List<Overlay> overlays = new List<Overlay>();
        foreach (PositionData pos in oPositions)
        {
            Overlay overlay = GetOverlayByID(pos.OverlayID);
            overlay.positions = pos;
            overlays.Add(overlay);
        }
        return overlays;
    }

    /// <summary>
    /// returns the the ID of an overlay if it exists
    /// </summary>
    /// <param name="name">name of overlay to look for</param>
    /// <returns>-1 if null</returns>
    public int GetOverlayIDByName(string name)
    {
        Overlay o = overlayList.Find(X => X.Name == name);
        if (o != null)
            return o.ID;
        else
            return -1;
    }

    /// <summary>
    /// Only adds new overlays, if overlay has same name as a already existing one, it wont be added.
    /// </summary>
    /// <param name="oOverlay">Overlay to add.</param>
    public void AddOverlay(Overlay oOverlay)
    {
        if (overlayList.Find(X => X.Name == oOverlay.Name) == null)
            overlayList.Add(oOverlay);
    }

    /// <summary>
    /// Updates specified overlay.
    /// </summary>
    /// <param name="oOverlay">Overlay to update.</param>
    public void UpdateOverlay(Overlay oOverlay)
    {
        int i = overlayList.FindIndex(X => X.ID == oOverlay.ID);
        overlayList[i].Name = oOverlay.Name;
        overlayList[i].positions = oOverlay.positions;
    }

    /// <summary>
    /// Deletes specified overlay.
    /// </summary>
    /// <param name="oOverlay">Overlay to delete.</param>
    public void DeleteOverlay(Overlay oOverlay)
    {
        overlayList.Remove(oOverlay);
    }

    /// <summary>
    /// Saves overlays to file.
    /// </summary>
    public void Save()
    {
        SaveData<Overlay>(overlayList);
    }

    /// <summary>
    /// Returns a unique overlay ID
    /// </summary>
    /// <returns>Int ID</returns>
    public int GetUniqueID()
    {
        int high = 0;
        foreach (Overlay o in overlayList)
        {
            if (o.ID > high) high = o.ID;
        }
        return high + 1;
    }
}
