﻿using System.Collections.Generic;

public class MenuDataBase : AbstractDataBase {

    private static MenuDataBase _instance;
    public static MenuDataBase Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new MenuDataBase();
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    private List<RijschoolMenuItem> menuItemList;

    private MenuDataBase()
    {
        fileName = "MenuDB";
        menuItemList = LoadData<RijschoolMenuItem>();
        if (menuItemList == null)
            menuItemList = new List<RijschoolMenuItem>();
    }

    public List<RijschoolMenuItem> GetMenuItems()
    {
        return new List<RijschoolMenuItem>(menuItemList);
    }

    public RijschoolMenuItem GetMenuItemByID(int oID)
    {
        return menuItemList.Find(X => oID == X.ID);
    }

    public List<RijschoolMenuItem> GetMenuItemsByIDs(List<int> oIDs)
    {
        List<RijschoolMenuItem> returnlist = new List<RijschoolMenuItem>();
        foreach (int i in oIDs)
            returnlist.Add(GetMenuItemByID(i));
        return returnlist;
    }

    public void AddMenuItem(RijschoolMenuItem oRijschoolMenuItem)
    {
        menuItemList.Add(oRijschoolMenuItem);
    }

    public void UpdateMenuItem(RijschoolMenuItem oRijschoolMenuItem)
    {
        int i = menuItemList.FindIndex(X => oRijschoolMenuItem.ID == X.ID);
        menuItemList[i].parentID = oRijschoolMenuItem.parentID;
        menuItemList[i].children = oRijschoolMenuItem.children;
        menuItemList[i].selectAction = oRijschoolMenuItem.selectAction;
        menuItemList[i].IsRoot = oRijschoolMenuItem.IsRoot;
    }

    public void DeleteMenuItem(RijschoolMenuItem oMenuItem)
    {
        if (oMenuItem.HasChildren())
            oMenuItem.RemoveAllChildren();
        if (oMenuItem.HasParent())
            menuItemList.Find(X => X.ID == oMenuItem.parentID).RemoveChild(oMenuItem);
        menuItemList.Remove(oMenuItem);
        Save();
    }

    public void Save()
    {
        SaveData<RijschoolMenuItem>(menuItemList);
    }

    /// <summary>
    /// returns the next unused ID.
    /// </summary>
    /// <returns>Unused ID.</returns>
    public int GetUniqueID()
    {
        int high = 0;
        foreach (RijschoolMenuItem r in menuItemList)
        {
            if (r.ID > high) high = r.ID;
        }
        return high + 1;
    }

    /// <summary>
    /// Returns the root menu item.
    /// </summary>
    /// <returns>Rijschoolmenuitem object.</returns>
    public RijschoolMenuItem GetRootMenuItem()
    {
        RijschoolMenuItem menuItem = menuItemList.Find(X => X.IsRoot);
        return menuItem;
    }
}
