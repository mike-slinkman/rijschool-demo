﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public abstract class AbstractDataBase {

    [HideInInspector]
    protected string fileName;

    protected List<T> LoadData<T>()
    {
        List<T> items = new List<T>();
        string json = "";

        string _filePath = GetPath();

        if (!File.Exists(_filePath))
        {
            if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "Databases")))
            {
                Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "Databases"));
            }
            Debug.Log("resource with name " + fileName + " does not exist!");
            File.CreateText(_filePath).Close();
            Debug.Log("created " + fileName);
        }

        json = File.ReadAllText(_filePath);

        if (json == "")
        {
            Debug.Log("resource with name " + fileName + "is empty!");
            return null;
        }
        items = JsonConvert.DeserializeObject<List<T>>(json);
        return items;
    }

    protected void SaveData<T>(List<T> itemList)
    {
        string _filePath = GetPath();

        string json = JsonConvert.SerializeObject(itemList);
        if (!File.Exists(_filePath))
            File.CreateText(_filePath).Close();
        File.WriteAllText(_filePath, json);
    }

    /// <summary>
    /// WARNING! CAN NOT BE UNDONE!
    /// </summary>
    protected void DeleteData()
    {
        string _filepath = GetPath();
        if (File.Exists(_filepath))
            File.Delete(_filepath);
    }

    private string GetPath()
    {
        string _filePath = "";
        _filePath = Path.Combine(Application.persistentDataPath, "Databases");
        _filePath = Path.Combine(_filePath, fileName + ".json");
        return _filePath;
    }
}
