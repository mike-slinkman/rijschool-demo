﻿using System.Collections.Generic;
using UnityEngine;
using Save;

public class SaveGameDataBase : AbstractDataBase {
    private List<PlayerData> Savegame;

    private static SaveGameDataBase _instance;
    public static SaveGameDataBase Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new SaveGameDataBase();
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    private SaveGameDataBase()
    {
        fileName = "SaveGame";
        Savegame = LoadData<PlayerData>();
        if (Savegame == null)
        {
            Savegame = new List<PlayerData>();
            PlayerData playerData = new PlayerData();
            Savegame.Add(playerData);
            Save();
        }
    }

    /// <summary>
    /// Save the difficulty score for an event in a scenario.
    /// </summary>
    /// <param name="iScenarioID">Scenario that has been played.</param>
    /// <param name="iEventID">Event to change the difficulty for.</param>
    /// <param name="iSkill">new difficulty score.</param>
    public void SaveDifficulty(int iScenarioID, int iEventID, int iSkill)
    {
        int skillObjectIndex = -1;
        if (Savegame[0].SkillList == null)
        {
            Debug.Log("difficultylist is null!");
        }
        else
        {
            skillObjectIndex = Savegame[0].SkillList.FindIndex(X => X.ScenarioID == iScenarioID && X.EventID == iEventID);
        }

        if (skillObjectIndex >= 0)
        {

            Savegame[0].SkillList[skillObjectIndex].SkillLevel = iSkill;
        }
        else
        {
            SkillObject difficultyObject = new SkillObject();
            difficultyObject.ScenarioID = iScenarioID;
            difficultyObject.EventID = iEventID;
            difficultyObject.SkillLevel = iSkill;
            Savegame[0].SkillList.Add(difficultyObject);
        }
        Save();
    }


    /// <summary>
    /// Get difficulty value for specific event in a scenario.
    /// </summary>
    /// <param name="iScenarioID">Scenario you are going to play.</param>
    /// <param name="iEventID">Event to get the difficulty for.</param>
    /// <returns>The difficulty value for specified event.</returns>
    public int GetSkillLevelForEvent(int iScenarioID, int iEventID)
    {
        int result = 0;
        
        if (Savegame.Count > 0 && Savegame[0].SkillList != null)
        {
            SkillObject SkillObject = Savegame[0].SkillList.Find(X => X.ScenarioID == iScenarioID && X.EventID == iEventID);
            if (SkillObject != null)
            {
                result = SkillObject.SkillLevel;
            }
        }

        return result;
    }

    public void SetSkillAverageForScenario(int iScenario, float fSkillaverage)
    {
        int averageObjectIndex = -1;
        if (Savegame[0].AverageList == null)
        {
            Debug.Log("averagelist is null!");
        }
        else
        {
            averageObjectIndex = Savegame[0].AverageList.FindIndex(X => X.ScenarioID == iScenario);
        }

        if (averageObjectIndex >= 0)
        {

            Savegame[0].AverageList[averageObjectIndex].SkillAverage = fSkillaverage;
        }
        else
        {
            SkillAverageObject skillAverageObject = new SkillAverageObject();
            skillAverageObject.SkillAverage = fSkillaverage;
            skillAverageObject.ScenarioID = iScenario;
            Savegame[0].AverageList.Add(skillAverageObject);
        }
        Save();
    }

    public float GetSkillAverageForScenario(int iScenarioID)
    {
        float result = 0;

        if (Savegame.Count > 0 && Savegame[0].AverageList != null)
        {
            SkillAverageObject SkillObject = Savegame[0].AverageList.Find(X => X.ScenarioID == iScenarioID);
            if (SkillObject != null)
            {
                result = SkillObject.SkillAverage;
            }
        }

        return result;
    }

    /// <summary>
    /// Saves the playerdata in this savegame.
    /// </summary>
    public void Save()
    {
        SaveData<PlayerData>(Savegame);
    }
}

namespace Save
{
    /// <summary>
    /// place all important playerdata in the save data class.
    /// </summary>
    public class PlayerData
    {
        public List<SkillObject> SkillList = new List<SkillObject>();
        public List<SkillAverageObject> AverageList = new List<SkillAverageObject>();
    }

    public class SkillObject
    {
        public int ScenarioID;
        public int EventID;
        public int SkillLevel;
    }

    public class SkillAverageObject
    {
        public int ScenarioID;
        public float SkillAverage;
    }
}
