﻿using System.Collections.Generic;
using UnityEngine;

public class EventDataBase : AbstractDataBase {

    private List<Event> eventList;
	
    /// <summary>
    /// Creates a new EventDataBase with the name specified
    /// </summary>
    /// <param name="oFileName">Name that the database should have</param>
    public EventDataBase(string oFileName)
    {
        fileName = oFileName;
        Debug.Log(oFileName);
        eventList = LoadData<Event>();
        if (eventList == null)
            eventList = new List<Event>();
    }

    /// <summary>
    /// Returns event based on the frame that it should execute on.
    /// </summary>
    /// <param name="oFrame">Frame in which the event should execute</param>
    /// <returns>Event that executes on specified frame</returns>
    public Event GetEventByFrameNumber(int oFrame)
    {
        return eventList.Find(X => X.Frame == oFrame);
    }

    /// <summary>
    /// Get all events.
    /// </summary>
    /// <returns>List of events.</returns>
    public List<Event> GetEvents()
    {
        return new List<Event>(eventList);
    }
    
    /// <summary>
    /// Adds the specified object to the database. Call Save() to make permanent.
    /// </summary>
    /// <param name="oEvent">Event to add.</param>
    public void AddEvent(Event oEvent)
    {
        eventList.Add(oEvent);
    }

    /// <summary>
    /// Updates the specified object in the database. Call Save() to make permanent.
    /// </summary>
    /// <param name="oEvent">Event to update.</param>
    public void UpdateEvent(Event oEvent)
    {
        int i = eventList.FindIndex(X => X.Frame == oEvent.Frame);
        eventList[i].FreezeVideo = oEvent.FreezeVideo;
        eventList[i].IsPopUp = oEvent.IsPopUp;
        eventList[i].ImageName = oEvent.ImageName;
        eventList[i].Header = oEvent.Header;
        eventList[i].Message = oEvent.Message;
    }
    
    /// <summary>
    /// Deletes the specified object from the database. Call Save() to make permanent.
    /// </summary>
    /// <param name="oEvent">Object to delete</param>
    public void DeleteEvent(Event oEvent)
    {
        eventList.Remove(oEvent);
    }
    
    /// <summary>
    /// Saves the content of this database object to file.
    /// </summary>
    public void Save()
    {
        SaveData<Event>(eventList);
    }

    /// <summary>
    /// WARNING! DELETES DATABASE. CAN NOT BE UNDONE!
    /// </summary>
    public void DevDeleteDataBase(bool sure = false)
    {
        if (sure)
            DeleteData();
    }

    /// <summary>
    /// Returns a unique event ID.
    /// </summary>
    /// <returns>Int ID.</returns>
    public int GetUniqueID()
    {
        int high = 0;
        foreach (Event e in eventList)
        {
            if (e.ID > high) high = e.ID;
        }
        return high + 1;
    }
}
