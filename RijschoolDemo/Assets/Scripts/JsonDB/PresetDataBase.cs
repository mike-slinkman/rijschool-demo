﻿using System.Collections.Generic;
using System;

[Serializable]
public class PresetDataBase : AbstractDataBase {

    private static PresetDataBase _instance;
    public static PresetDataBase Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PresetDataBase();
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    private List<Preset> presetList;

    private PresetDataBase()
    {
        fileName = "Presets";
        presetList = LoadData<Preset>();
        if (presetList == null)
            presetList = new List<Preset>();
    }

    /// <summary>
    /// Returns preset with specified ID.
    /// </summary>
    /// <param name="oID">ID to look for.</param>
    /// <returns>preset with ID.</returns>
    public Preset GetPresetByID(int oID)
    {
        return presetList.Find(X => X.ID == oID);
    }

    /// <summary>
    /// Get all presets.
    /// </summary>
    /// <returns>list of all presets.</returns>
    public List<Preset> GetPresets()
    {
        return new List<Preset>(presetList);
    }

    /// <summary>
    /// Add preset to the database.
    /// </summary>
    /// <param name="oPreset">Preset to add.</param>
    public void AddPreset(Preset oPreset)
    {
        presetList.Add(oPreset);
    }

    /// <summary>
    /// Updates a preset based on the ID the parameter has.
    /// </summary>
    /// <param name="oPreset">Preset with new info.</param>
    public void UpdatePreset(Preset oPreset)
    {
        int i = presetList.FindIndex(X => X.ID == oPreset.ID);
        presetList[i].Name = oPreset.Name;
        presetList[i].GoalList = oPreset.GoalList;
    }

    /// <summary>
    /// Deletes specified preset.
    /// </summary>
    /// <param name="oPreset">Preset to delete.</param>
    public void DeletePreset(Preset oPreset)
    {
        presetList.Remove(oPreset);
    }

    /// <summary>
    /// Saves the database to file.
    /// </summary>
    public void Save()
    {
        SaveData<Preset>(presetList);
    }

    /// <summary>
    /// returns a unique preset ID.
    /// </summary>
    /// <returns>Int ID.</returns>
    public int GetUniqueID()
    {
        int high = 0;
        foreach (Preset preset in presetList)
        {
            if (preset.ID > high) high = preset.ID;
        }
        return high + 1;
    }
    
}
