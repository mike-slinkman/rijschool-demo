﻿using System.Collections;
using UnityEngine;

public class ScenelistDownloader : MonoBehaviour {
    bool downloading = false;
	// Use this for initialization
	void Start () {
        StartCoroutine(DownloadRequiredFiles());
    }

    IEnumerator DownloadRequiredFiles()
    {
        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Scenarios, "ScenarioDB", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Scenarios, "OverlayDB", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Scenarios, "Presets", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Scenarios, "MenuDB", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Image, "ob09-600x300", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Image, "a01_30", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Image, "d01", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Image, "b1", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        downloading = true;
        DownloadManager.instance.Download(DownloadManager.DownloadType.Image, "g03", CurrentDownloadDone);
        yield return new WaitUntil(() => !downloading);

        Debug.Log("Downloads done!");
        DownloadsDone();
    }

    void CurrentDownloadDone()
    {
        downloading = false;
    }

    void DownloadsDone()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Menu")
        {
            //old menu
        }
        else
        {
            MenuManager menuBuilderScript = FindObjectOfType<MenuManager>();
            RijschoolMenuItem menuroot = MenuDataBase.Instance.GetRootMenuItem();
            menuBuilderScript.BuildMenu(menuroot);
        }
    }
}
