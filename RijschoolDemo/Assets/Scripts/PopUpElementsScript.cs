﻿using UnityEngine;
using UnityEngine.UI;

public class PopUpElementsScript : MonoBehaviour {

    // UI elements
    [SerializeField]
    private Text headerText;
    public Text HeaderText
    {
        get { return headerText; }
    }

    [SerializeField]
    private Text messageText ;
    public Text MessageText
    {
        get { return messageText; }
    }

    [SerializeField]
    private Image messageImage ;
    public Image MessageImage
    {
        get { return messageImage; }
    }

    [SerializeField]
    private Image button ;
    public Image Button
    {
        get { return button; }
    }

    [SerializeField]
    private Image buttonOverlay ;
    public Image ButtonOverlay
    {
        get { return buttonOverlay; }
    }

    [SerializeField]
    private Text buttonText ;
    public Text ButtonText
    {
        get { return buttonText; }
    }

    [SerializeField]
    private Image longBG ;
    public Image LongBG
    {
        get { return longBG; }
    }

    [SerializeField]
    private Image shortBG ;
    public Image ShortBG
    {
        get { return shortBG; }
    }

}
