﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EventManager : MonoBehaviour {

    //singleton var
    [HideInInspector]
    public static EventManager instance = null;

    [HideInInspector]
    public delegate void OnEventCompleted();
    [HideInInspector]
    public OnEventCompleted onEventCompleted;

    List<Event> activeEventList = new List<Event>();

    //make this a singleton
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        if (VideoPlayerScript.instance != null)
        {
            VideoPlayerScript.instance.onEventFrameEntered += StartEventFrame;
            VideoPlayerScript.instance.onFrameEntered += FrameEntered;
        }

        foreach (TimedButtonScript overlayScript in GameObject.FindObjectsOfType<TimedButtonScript>())
        {
            overlayScript.onTimerCompleted += InteractedWithOverlay;
        }
    }

    void Update()
    {
        for (int i = activeEventList.Count; i > 0; i--)
        {
            activeEventList[i - 1].Tick(Time.deltaTime);
            if (activeEventList[i-1].MarkedForDeletion)
            {
                Debug.Log("Deleted event!");
                activeEventList.RemoveAt(i - 1);
            }
        }
    }

    /// <summary>
    /// Call from overlay when looked at.
    /// </summary>
    /// <param name="overlayName">Name of overlay that was looked at.</param>
    public void InteractedWithOverlay(string overlayName)
    {
        activeEventList = activeEventList.OrderByDescending(X => X.CurrentStage).ToList();
        int beststage = 0;
        if (activeEventList.Count != 0)
            beststage = activeEventList[0].CurrentStage;
        Debug.Log("currently running " + activeEventList.Count + " events with beststage: " + beststage);
        string debugtextCurrentEvents = "Current events";
        if (activeEventList.Find(X => X.EventType == Event.FrameEventType.Quiz) == null) // checks if quizes are active, if so don't check for goals on running events.
        {
            for (int i = activeEventList.Count; i > 0; i--)
            {
                if (activeEventList[i - 1].CurrentStage == beststage)
                {
                    debugtextCurrentEvents += (" : " + activeEventList[i - 1].Header);
                    activeEventList[i - 1].CheckForGoals(overlayName);
                }
            }
        }
        Debug.Log(debugtextCurrentEvents);
    }

    void StartEventFrame(Event frameEvent)
    {
        activeEventList.Add(frameEvent);
        frameEvent.Activate();
    }

    void FrameEntered(int pFrame)
    {
        for (int i = activeEventList.Count; i > 0; i--)
        {
            if (activeEventList[i-1].EndFrame == pFrame)
            {
                Debug.Log("Didnt finish event on time, allemaal een onvoldoende!");
                activeEventList[i-1].Done(false);
            }
        }
    }
}
