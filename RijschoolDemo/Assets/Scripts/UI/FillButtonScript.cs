﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillButtonScript : AbstractFillButton
{

    // filling vars
    [SerializeField]
    protected string activateValue = "";

    public delegate void OnfillCompleted(GameObject btn, string buttonName = "");
    public OnfillCompleted onfillCompleted;

    protected override void OnFill()
    {
        if (onfillCompleted != null)
        {
            onfillCompleted(this.gameObject, (activateValue == "") ? gameObject.name : activateValue);
        }
    }
}
