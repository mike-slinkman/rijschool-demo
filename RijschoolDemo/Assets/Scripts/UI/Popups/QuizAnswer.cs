﻿public class QuizAnswer {

    public string AnswerText { get; set; }
    public bool IsCorrect { get; set; }

    public QuizAnswer (string answer, bool isCorrect) {
        AnswerText = answer;
        IsCorrect = isCorrect;
    }
    
}
