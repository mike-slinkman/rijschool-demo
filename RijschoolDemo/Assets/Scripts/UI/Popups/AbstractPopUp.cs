﻿using UnityEngine;
using DG.Tweening;

public abstract class AbstractPopUp : MonoBehaviour {

    public bool MarkedForDeletion { get; set; }
    protected Event frameEvent;
    protected PopUpElementsScript UIElements;
    protected bool hasButton = false;


    /// <summary>
    /// Prepairs the popup and shows it on the screen.
    /// </summary>
    /// <param name="oFrameEvent">event with the information needed for this popup.</param>
    /// <param name="oUIElements">UI object for this popup</param>
    public void InitPopUp(Event oFrameEvent, PopUpElementsScript oUIElements)
    {
        frameEvent = oFrameEvent;
        UIElements = oUIElements;

        //set pop up text
        UIElements.HeaderText.text = frameEvent.Header;
        UIElements.MessageText.text = frameEvent.Message;
        if (frameEvent.EventType == Event.FrameEventType.ContinueButton || frameEvent.EventType == Event.FrameEventType.StillGame)
            hasButton = true;

        //set image
        if (frameEvent.ImageName != "")
            UIElements.MessageImage.sprite = Resources.Load<Sprite>("PopUpSprites/" + frameEvent.ImageName);

        //disable buttons and set correct BG
        UIElements.Button.gameObject.SetActive(hasButton);
        UIElements.ButtonOverlay.gameObject.SetActive(hasButton);
        UIElements.ButtonText.gameObject.SetActive(hasButton);
        UIElements.LongBG.gameObject.SetActive(hasButton);
        UIElements.ShortBG.gameObject.SetActive(!hasButton);
        AfterInitPopUp();
        VisualizePopUp();
        AfterVisualizePopUp();
    }

    /// <summary>
    /// last step before showing the popup. use this function to set any specifics for the type of popup you are making.
    /// </summary>
    protected virtual void AfterInitPopUp() { }
    /// <summary>
    /// called after popup is visualized. use this function to set any specifics for the popup that require the gameobject to be active.
    /// </summary>
    protected virtual void AfterVisualizePopUp() { }

    protected void VisualizePopUp()
    {
        SFXManager.instance.PlaySFX("newPopUp");
        gameObject.SetActive(true);
        gameObject.transform.DOLocalMoveX(-10, 1f).From();
    }

    /// <summary>
    /// removes event from eventlist and marks object for deletion.
    /// </summary>
    public void Remove(bool CalledByFrameEvent = false, bool passed = false)
    {
        MarkedForDeletion = true;
        if (!CalledByFrameEvent)
            frameEvent.Done(passed);
    }
}
