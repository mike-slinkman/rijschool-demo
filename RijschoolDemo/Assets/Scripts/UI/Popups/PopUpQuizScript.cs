﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PopUpQuizScript : AbstractPopUp {

	List<AnswerScript> answers = new List<AnswerScript> ();

    protected override void AfterVisualizePopUp()
    {
        Debug.Log("quiz opbouwen");
        UIElements.LongBG.gameObject.SetActive(false);
        UIElements.ShortBG.gameObject.SetActive(false);
        UIElements.MessageImage.gameObject.SetActive(false);

        int i = 0;
        foreach (QuizAnswer qq in frameEvent.QuizAnswers)
        {
            GameObject answer = (GameObject)Instantiate(Resources.Load("PopUpHolderQuizAnswer"), GameObject.FindGameObjectWithTag("AnswersHolder").transform, false);
            answer.transform.Translate(new Vector3(0f, i*-0.9f, 0f));
            answer.GetComponent<AnswerScript>().SetAnswerInfo(frameEvent.QuizAnswers[i], this);
			answers.Add(answer.GetComponent<AnswerScript>());
            i++;
        }
    }

    public void AnswerSelected(AnswerScript chosenAnswer)
    {
        foreach(AnswerScript answer in answers)
        {
            answer.VisualizePopOut(answer == chosenAnswer);
        }

        StartCoroutine(WaitAndremove(chosenAnswer.IsCorrect));
    }
    
    public IEnumerator WaitAndremove(bool passed)
    {
        yield return new WaitForSeconds(2);

        Remove(false, passed);
    }

}
