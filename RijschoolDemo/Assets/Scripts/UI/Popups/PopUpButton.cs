﻿using UnityEngine;

public class PopUpButton : AbstractPopUp {

    protected override void AfterInitPopUp()
    {
        UIElements.ButtonOverlay.GetComponent<FillButtonScript>().onfillCompleted += OnButtonInteract;
    }

    /// <summary>
    /// user has interacted with the continue button
    /// </summary>
    public void OnButtonInteract(GameObject obj, string buttonName = "continue")
    {
        Debug.Log("user interacted with button: " + buttonName);
        SFXManager.instance.PlaySFX("completedPopUp");
        Remove();
    }
}
