﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBackButtonScript : AbstractFillButton {

    // Use this for initialization
    MenuManager menu;

    protected void Start()
    {
        menu = FindObjectOfType<MenuManager>();
        lockedWhileDownloading = true;
    }

    protected override void OnFill()
    {
        menu.OnBack();
    }
}
