﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrollButtonScript : AbstractFillButton, IGvrPointerHoverHandler
{
    float scrollspeed = 0f;
    MenuManager menuBuilderScript;
    public bool left;
    private void Start()
    {
        menuBuilderScript = FindObjectOfType<MenuManager>();
    }
    
    protected override void Fill()
    {
        if (left)
            menuBuilderScript.ScrollLeft(scrollspeed * Time.deltaTime);
        else
            menuBuilderScript.ScrollRight(scrollspeed * Time.deltaTime);
    }
    
    public void OnGvrPointerHover(PointerEventData eventData)
    {
        Vector3 worldhit = eventData.pointerCurrentRaycast.worldPosition;
        RectTransform rt = GetComponent<RectTransform>();
        float width = rt.sizeDelta.x * rt.localScale.x;
        float tempspeed = transform.position.x - worldhit.x + width/2;
        scrollspeed = Mathf.Abs(1 / width * tempspeed - 1);
    }

    protected override void LateGazeOut()
    {
        menuBuilderScript.CenterOnNearestItem();
    }
}
