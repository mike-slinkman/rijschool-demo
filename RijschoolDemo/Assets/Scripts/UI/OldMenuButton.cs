﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldMenuButton : AbstractFillButton {

    public int scenario;
    public void Start()
    {
        lockedWhileDownloading = true;
    }

    protected override void OnFill()
    {
        ScenarioDataBase dataBase = ScenarioDataBase.Instance;
        StartCoroutine(dataBase.GetScenarioByID(scenario).Play());
    }
    
}
