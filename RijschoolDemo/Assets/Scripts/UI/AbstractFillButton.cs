﻿using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractFillButton : MonoBehaviour {

    protected bool startFill;
    [SerializeField]
    protected Image fillBar;
    [SerializeField]
    protected float fillTime = 1f;
    private bool locked;
    public bool Locked{
        get { return locked; }
        set {
            locked = value;
            if (value == false && onGazeOutCalled)
            {
                OnGazeOut();
            }
        } }
    private bool onGazeOutCalled = false;
    protected bool lockedWhileDownloading = false;
    DownloadManager downloadManager;

    protected bool buttonFilled;

    void Awake()
    {
        Locked = false;
        if (fillBar == null)
            fillBar = gameObject.GetComponent<Image>();
    }

    protected void Start()
    {
        downloadManager = FindObjectOfType<DownloadManager>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (lockedWhileDownloading && downloadManager != null && downloadManager.IsDownloading)
            Locked = true;
        else
            Locked = false;

        if (startFill && fillBar != null)
        {
            fillBar.fillAmount += Time.deltaTime / fillTime;
            if (fillBar.fillAmount >= 1)
            {
                if (!buttonFilled)
                {
                    buttonFilled = true;
                    OnFill();
                }
                Fill();
            }
        }
    }

    /// <summary>
    /// Called once when button is filled (gaze version of onclick())
    /// </summary>
    protected virtual void OnFill() { }

    /// <summary>
    /// Called when button is filled (gaze version of click())
    /// </summary>
    protected virtual void Fill() { }

    /// <summary>
    /// Called when GazeOver is done
    /// </summary>
    protected virtual void LateGazeOver() { }

    /// <summary>
    /// Called when GazeOut is done
    /// </summary>
    protected virtual void LateGazeOut() { }

    // Update is called once per frame
    public void OnGazeOver()
    {
        if (locked)
        {
            onGazeOutCalled = false;
        }
        else
        {
            startFill = true;
        }
        LateGazeOver();
    }

    public void OnGazeOut()
    {
        if (locked)
        {
            onGazeOutCalled = true;
        }
        else
        {
            buttonFilled = false;
            startFill = false;
            fillBar.fillAmount = 0;
        }
        LateGazeOut();
    }
}
