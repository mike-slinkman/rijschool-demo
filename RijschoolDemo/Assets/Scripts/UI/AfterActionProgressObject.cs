﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AfterActionProgressObject : MonoBehaviour {
    
    [SerializeField]
    private Image originalValue;
    [SerializeField]
    private RectTransform difference;
    [SerializeField]
    private Image positiveDifference;
    [SerializeField]
    private Image negativeDifference;
    [SerializeField]
    private Image mediumposition;
    [SerializeField]
    private Image HardPosition;
    [SerializeField]
    private RectTransform container;
    

    public void Initialize(Event oldScores, Event newScores)
    {
        difference.gameObject.SetActive(true);
        //Debug.Log("container.sizedelta: " + container.sizeDelta + " oldscores medium: " + oldScores.SkillThresholdMedium + " on object with id: " + gameObject.GetInstanceID());
        //Vector3 testvector = new Vector3(mediumposition.transform.position.x, ((container.sizeDelta.y / 100 * oldScores.SkillThresholdMedium) - container.sizeDelta.y / 2), mediumposition.transform.position.z);
        //Debug.Log(testvector);
        mediumposition.transform.localPosition = new Vector3(mediumposition.transform.position.x, ((container.sizeDelta.y / 100 * oldScores.SkillThresholdMedium) - container.sizeDelta.y/2), mediumposition.transform.position.z);
        HardPosition.transform.localPosition = new Vector3(HardPosition.transform.position.x, ((container.sizeDelta.y / 100 * oldScores.SkillThresholdHard) - container.sizeDelta.y / 2), HardPosition.transform.position.z);
        //originalValue.fillAmount = (float)oldScores.Difficulty / 100f;
        Sequence sequence = DOTween.Sequence();
        sequence.Append(DOTween.To(() => originalValue.fillAmount, x => originalValue.fillAmount = x, (float)oldScores.Difficulty / 100f, 2));

        difference.localPosition = new Vector3(difference.localPosition.x, oldScores.Difficulty * 3, difference.localPosition.z); //TODO 3 dynamisch maken op basis van container height
        //Debug.Log("Oldscore: " + oldScores.Difficulty + " newscore: " + newScores.Difficulty);
        if (oldScores.Difficulty < newScores.Difficulty)
        {
            //went up in score
            negativeDifference.gameObject.SetActive(false);
            positiveDifference.gameObject.SetActive(true);
            sequence.Append(DOTween.To(() => positiveDifference.fillAmount, x => positiveDifference.fillAmount = x, ((float)newScores.Difficulty - (float)oldScores.Difficulty) / 100, 2));
            //positiveDifference.fillAmount = ((float)newScores.Difficulty - (float)oldScores.Difficulty) / 100;
        }
        else
        {
            //went down in score
            negativeDifference.gameObject.SetActive(true);
            positiveDifference.gameObject.SetActive(false);
            sequence.Append(DOTween.To(() => negativeDifference.fillAmount, x => negativeDifference.fillAmount = x, ((float)oldScores.Difficulty - (float)newScores.Difficulty) / 100, 2));
            //negativeDifference.fillAmount = ((float)oldScores.Difficulty - (float)newScores.Difficulty) / 100;
        }
    }
}
