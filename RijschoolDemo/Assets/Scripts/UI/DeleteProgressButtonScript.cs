﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DeleteProgressButtonScript : AbstractFillButton {

    DownloadManager downloadManager;

    protected void Start()
    {
        lockedWhileDownloading = true;
    }
    protected override void OnFill()
    {
        File.Delete(Path.Combine(Application.persistentDataPath, Path.Combine("Databases", "SaveGame.json")));
        SFXManager.instance.PlaySFX("completedPopUp");
    }
}
