﻿using UnityEngine;

public class MenuFillButtonScript : AbstractFillButton {

    // Use this for initialization
    MenuManager menu;

    new protected void Start()
    {
        base.Start();
        menu = FindObjectOfType<MenuManager>();
        lockedWhileDownloading = true;
    }

    protected override void OnFill()
    {
        menu.OnSelect();
    }
}
