﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerformanceReportScript : MonoBehaviour {

    public Text performancetext;

	// Use this for initialization
	void Start () {
        PerformanceChecker performanceChecker = GameObject.FindObjectOfType<PerformanceChecker>();
        if (performanceChecker != null)
        {
            string reporttext = "";
            reporttext += ".1% low: " + performanceChecker.GetPointOnePercentLow() + "fps \n ";
            reporttext += "1% low: " + performanceChecker.GetOnePercentLow() + "fps \n ";
            reporttext += "average: " + performanceChecker.GetAverage() + "fps \n ";
            reporttext += ".1% high: " + performanceChecker.GetOnePercentHigh() + "fps \n ";
            reporttext += ".1% high: " + performanceChecker.GetPointOnePercentHigh() + "fps \n ";
            performancetext.text = reporttext;
        }
        else
        {
            performancetext.text = "No metrics available";
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
