﻿using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour {
    List<AbstractPopUp> currentPopUps = new List<AbstractPopUp>();
    public static PopupManager instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
        if (currentPopUps.Count > 0)
        {
            for (int i = currentPopUps.Count; i > 0; i--)
            {
                if (currentPopUps[i-1].MarkedForDeletion)
                {
                    AbstractPopUp p = currentPopUps[i-1];
                    currentPopUps.Remove(p);
                    Destroy(p.gameObject);
                }
            }
        }
    }

    public AbstractPopUp CreatePopup(Event oFrameEvent)
    {
        if (currentPopUps.Count > 0)
            MarkAllForDeletion();
        //create new event 
        string objToCreate = "";
        if (oFrameEvent.EventType == Event.FrameEventType.Quiz)
            objToCreate = "PopUpHolderQuiz";
        else
            objToCreate = (oFrameEvent.ImageName == "") ? "PopUpHolderTextOnly" : "PopUpHolderImage";

        GameObject popUpObject = (GameObject)Instantiate(Resources.Load(objToCreate), GameObject.FindGameObjectWithTag("PopUpHolder").transform, false);

        //add a script and init pop up, for later 
        PopUpElementsScript UI = popUpObject.GetComponent<PopUpElementsScript>();
        AddScript(popUpObject, oFrameEvent).InitPopUp(oFrameEvent, UI);
        return currentPopUps[currentPopUps.Count-1];
    }

    private AbstractPopUp AddScript(GameObject obj, Event oEvent)
    {
        AbstractPopUp popUp;
        switch (oEvent.EventType)
        {
            case Event.FrameEventType.Quiz:
                popUp = obj.AddComponent<PopUpQuizScript>();
                break;
            case Event.FrameEventType.ContinueButton:
                popUp = obj.AddComponent<PopUpButton>();
                break;
            case Event.FrameEventType.Preset:
            case Event.FrameEventType.Custom:
                popUp = obj.AddComponent<PopUpLookAtEvent>();
                break;
            default:
                popUp = obj.AddComponent<PopUpButton>();
                break;
        }
        currentPopUps.Add(popUp);
        return popUp;
    }

    private void MarkAllForDeletion()
    {
        foreach (AbstractPopUp p in currentPopUps)
        {
            p.MarkedForDeletion = true;
        }
    }

    public void DestroyPopUp(AbstractPopUp oAbstractPopUp)
    {
        oAbstractPopUp.MarkedForDeletion = true;
    }
}
