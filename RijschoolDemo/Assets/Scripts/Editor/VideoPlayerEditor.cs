﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using UnityEditor.SceneManagement;

public class VideoPlayerEditor : EditorWindow {

    private static VideoPlayerEditor instance;

    static VideoPlayer videoplayer;
    private bool frameBased = true;

    [MenuItem("VRRijschool/Video player")]
    public static void ShowEditorWindow()
    {
        Type inspectorType = Type.GetType("UnityEditor.Console, UnityEditor.dll");
        instance = GetWindow<VideoPlayerEditor>("Video player", true, inspectorType);
        
        instance.Init();
    }

    private void Init()
    {
        Debug.Log("Init called on video player script");
        //EditorSceneManager.OpenScene("Assets/Scenes/EditorScene.unity");
        videoplayer = FindObjectOfType<VideoPlayer>();
        if (videoplayer == null)
        {
            Debug.Log("cant find VideoPlayerScript, make sure it exists in the EditorScene before you open the VideoPlayerEditor!");
            GetWindow<VideoPlayerEditor>().Close();
        }

        EditorDataManager.Instance.onEditScenarioChange += OnScenarioChange;
    }

    private void OnScenarioChange(Scenario scenario)
    {
        string url = "";
        url = Path.Combine(Application.persistentDataPath, "Videos"); //path.combine to make sure that it is platform independent
        url = Path.Combine(url, scenario.VideoName + ".mp4");
        videoplayer.url = url;
        videoplayer.Prepare();
    }

    private void OnInspectorUpdate()
    {
        Repaint();
    }

    private void OnGUI()
    {
        if (Application.isPlaying)
        {
            if (!DownloadManager.instance.IsDownloading)
            {
                if (videoplayer == null) Init();
                if (videoplayer.isPrepared)
                {
                    SimpleControls();
                }
                else
                    NoVideoSet();
            }
            else
            {
                GUILayout.Label("Downloading, please wait...");
            }
        }
        else
        {
            GUILayout.Label("Go into play mode to start editing!");
        }
    }

    private void NoVideoSet()
    {
        if (videoplayer.url == "")
            GUILayout.Label("Please select a scenario with a video attached to start editing!");
        else
            GUILayout.Label("Video not prepaired");
    }

    private void SimpleControls()
    {
        GUILayout.BeginHorizontal();
        {
            GUILayout.BeginHorizontal();
            {
                
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            {
                if (frameBased)
                {
                    int frame = Convert.ToInt32(videoplayer.frame);
                    frame = EditorGUILayout.IntSlider(frame, 1, Convert.ToInt32(videoplayer.frameCount));
                    if (frame != videoplayer.frame)
                        videoplayer.frame = frame;
                    EditorGUILayout.LabelField("0", GUILayout.Width(12));
                    EditorGUILayout.LabelField((videoplayer.frameCount).ToString(), GUILayout.Width(64));
                }
                else
                {
                    int time = Convert.ToInt32(videoplayer.frame / videoplayer.frameRate);
                    time = EditorGUILayout.IntSlider(time, 1, Convert.ToInt32(videoplayer.frameCount / videoplayer.frameRate));
                    if (Convert.ToInt32(time * videoplayer.frameRate) != videoplayer.frame)
                        videoplayer.frame = Convert.ToInt32(time * videoplayer.frameRate);
                    EditorGUILayout.LabelField("0", GUILayout.Width(12));
                    EditorGUILayout.LabelField((videoplayer.frameCount / videoplayer.frameRate).ToString(), GUILayout.Width(64));
                }
                EditorDataManager.Instance.CurrentFrame = Convert.ToInt32(videoplayer.frame);
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        {

            string playpause = "";
            playpause = videoplayer.isPlaying ? "Pause" : "Play";
            if (GUILayout.Button("-10", GUILayout.Width(32), GUILayout.MaxWidth(32)))
            {
                ChangeFrame(-10);
            }
            if (GUILayout.Button("-1", GUILayout.Width(32), GUILayout.MaxWidth(32)))
            {
                ChangeFrame(-1);
            }
            if (GUILayout.Button(playpause, GUILayout.Width(48), GUILayout.MaxWidth(48)))
            {
                if (videoplayer.isPlaying)
                    videoplayer.Pause();
                else
                    videoplayer.Play();
            }
            if (GUILayout.Button("+1", GUILayout.Width(32), GUILayout.MaxWidth(32)))
            {
                ChangeFrame(1);
            }
            if (GUILayout.Button("+10", GUILayout.Width(32), GUILayout.MaxWidth(32)))
            {
                ChangeFrame(10);
            }
        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        {
            frameBased = EditorGUILayout.Toggle("Frame based scrubbing", frameBased);
        }
        GUILayout.EndHorizontal();
    }

    private void ChangeFrame(int difference)
    {
        if (videoplayer.isPlaying)
            videoplayer.Pause();
        videoplayer.frame += difference;
        EditorDataManager.Instance.CurrentFrame = Convert.ToInt32(videoplayer.frame);
    }

    private void EndFocus()
    {
        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }
}
