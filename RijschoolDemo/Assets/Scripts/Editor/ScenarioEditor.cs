﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ScenarioEditor : EditorWindow {
    ScenarioDataBase dataBase;
    private int selectedID;
    Scenario editScenario;
    Vector2 scrollviewposition = new Vector2();
    bool ScenariosLoaded = false;
    bool isInitialized = false;

    private void Initialize()
    {
        if (EditorApplication.isPlaying)
        {
            EditorApplication.playModeStateChanged += OnPlayModeChange;
            EditorDataManager.Instance.onDownloadsDone += OnDownloadsDone;
            isInitialized = true;
            dataBase = ScenarioDataBase.Instance;
            ScenariosLoaded = true;
        }
    }

    [MenuItem("VRRijschool/Scenario Editor")]
    public static void ShowEditorWindow()
    {
        Type inspectorType = Type.GetType("UnityEditor.SceneView,UnityEditor.dll");
        GetWindow<ScenarioEditor>("Scenario editor", true, inspectorType);
    }

    public void OnPlayModeChange(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredPlayMode)
        {
            Initialize();
        }
        if (state == PlayModeStateChange.ExitingPlayMode)
        {
            ScenariosLoaded = false;
            isInitialized = false;
        }
    }

    public void OnDownloadsDone(bool downloadsDone)
    {
        if (downloadsDone)
        {
            dataBase.ReloadData();
            Debug.Log("Reloaded the database!");
            ScenariosLoaded = true;
        }
        else
        {
            ScenariosLoaded = false;
        }
        Repaint();
    }

    private void OnGUI()
    {
        if (!isInitialized)
            Initialize();
        if (EditorApplication.isPlaying)
        {
            if (!DownloadManager.instance.IsDownloading)
            {
                if (ScenariosLoaded)
                {
                    // {} between begin and end of areas is for ease of use. not required :)
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(300));
                    {
                        EditorGUILayout.BeginVertical(GUILayout.Width(150));
                        {
                            BuildScrollableOverview();
                        }
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.BeginVertical(GUILayout.Width(200));
                        {
                            BuildScenarioView();
                            EditorGUILayout.LabelField("Click download to get the latest version of all files.");

                            if (GUILayout.Button("Download"))
                            {
                                DeleteEverything();
                                FindObjectOfType<DownloadForEditorScript>().DownloadEverything();
                            }
                        }
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndHorizontal();
                }
                else
                {
                    GUILayout.Label("Loading scenarios, please wait");
                }
            }
            else
            {
                GUILayout.Label("Downloading, please wait...");
            }
        }
        else
        {
            GUILayout.Label("Enter play mode to start editing!");
        }
    }

    private void BuildScrollableOverview()
    {
        if (GUILayout.Button("New Scenario"))
        {
            AddNew();
            EndFocus();
        }

        scrollviewposition = EditorGUILayout.BeginScrollView(scrollviewposition);
        {
            Debug.Log("scenarios: " + dataBase.GetScenarios().Count);
            foreach (Scenario scenario in dataBase.GetScenarios())
            {
                if (GUILayout.Button(scenario.Name))
                {
                    editScenario = scenario;
                    EndFocus();
                }
            }
        }
        EditorGUILayout.EndScrollView();
    }

    private void BuildScenarioView()
    {
        if (editScenario != null)
        {
            float oldlabelwidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 50;
            editScenario.Name = EditorGUILayout.TextField("Name:", editScenario.Name);
            editScenario.VideoName = EditorGUILayout.TextField("Video:", editScenario.VideoName);
            EditorGUIUtility.labelWidth = oldlabelwidth;

            GUILayout.Space(20);

            if (GUILayout.Button("Edit Content"))
            {
                EditorDataManager.Instance.EditScenario = editScenario;
            }
            GUILayout.Space(0);
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Save"))
                {
                    SaveEditedItem();
                    EndFocus();
                }
                if (GUILayout.Button("Delete"))
                {
                    DeleteEditedItem();
                    EndFocus();
                    editScenario = null;
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            EditorGUILayout.LabelField("Select a Scenario on the left to get started!");
        }

    }

    private void AddNew()
    {
        // show popup to enter scenario name. after name is entered a eventdb will be created and the new scenario will be added to the list
        NewScenarioPopup window = ScriptableObject.CreateInstance<NewScenarioPopup>();
        window.position = new Rect(Screen.width / 2, Screen.height / 2, 300, 300);
        window.ShowPopup();
    }

    private void SaveEditedItem()
    {
        dataBase.UpdateScenario(editScenario);
        dataBase.Save();
    }

    private void DeleteEditedItem()
    {
        dataBase.DeleteScenario(editScenario);
        dataBase.Save();
    }

    private void EndFocus()
    {
        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }
    
    private void DeleteEverything()
    {
        string videopath = Path.Combine(Application.persistentDataPath, "Videos");
        string jsonpath = Path.Combine(Application.persistentDataPath, "Databases");
        System.IO.DirectoryInfo di;
        if (Directory.Exists(videopath))
        {
            di = new DirectoryInfo(videopath);
            
            foreach (FileInfo file in di.GetFiles())
            {
                Debug.Log("Deleted: " + file.Name);
                file.Delete();
            }
        }
        if (Directory.Exists(jsonpath))
        {

            di = new DirectoryInfo(jsonpath);

            foreach (FileInfo file in di.GetFiles())
            {
                if (file.Name != "SaveGame.json")
                {
                    Debug.Log("Deleted: " + file.Name);
                    file.Delete();
                }
            }
        }
    }
}

public class NewScenarioPopup : EditorWindow {
    ScenarioDataBase dataBase;
    string _name = "";

    private void Awake()
    {
        dataBase = ScenarioDataBase.Instance;
    }

    private void OnGUI()
    {
        GUILayout.Label("Create a new scenario");
        _name = EditorGUILayout.TextField("Scenario name:", _name);
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Create"))
            {
                Scenario s = new Scenario(dataBase.GetUniqueID(), _name);
                dataBase.AddScenario(s);
                dataBase.Save();
                this.Close();
            }
            if (GUILayout.Button("Cancel"))
            {
                this.Close();
            }
        }
        GUILayout.EndHorizontal();
    }
}
