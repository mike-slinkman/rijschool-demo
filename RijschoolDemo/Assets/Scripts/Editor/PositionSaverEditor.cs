﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

[CustomEditor(typeof(PositionSaver))]
public class PositionSaverEditor : EditorWindow {
    static int popupint;
    static int selectedScenario = 0;
    private List<Scenario> scenariolist;
    Vector2 scrollviewposition = new Vector2();
    OverlayDataBase overlayDataBase;
    PositionDataDataBase positionDataDataBase;
    private bool isInitialized = false;
    Overlay editOverlay;

    List<Overlay> overlayList = new List<Overlay>();
    List<GameObject> gameobjectList = new List<GameObject>();

    private void Initialize()
    {
        if (Application.isPlaying)
        {
            EditorApplication.playModeStateChanged += OnPlayModeChange;
            EditorDataManager.Instance.onEditScenarioChange += LoadNewScenario;
            isInitialized = true;
        }
    }

    public void OnPlayModeChange(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredPlayMode)
        {
            Initialize();
        }
    }

    /// <summary>
    /// sets up the window with the specified scenario
    /// </summary>
    /// <param name="Scenario">chosen scenario</param>
    private void LoadNewScenario(Scenario Scenario)
    {
        Debug.Log("new scenario selected, loading overlays now");
        overlayDataBase = OverlayDataBase.Instance;
        positionDataDataBase = new PositionDataDataBase(Scenario.PositionDBID);
        editOverlay = null;
        scrollviewposition = new Vector2();
        overlayList = overlayDataBase.GetOverlayListForPositions(positionDataDataBase.GetPositionDatas());
        
        foreach (Overlay overlay in overlayList)
        {
            AddOverlayToScene(overlay);
        }
        Repaint();
    }

    public void OnGUI()
    {
        if (!isInitialized)
            Initialize();
        EditorStyles.textField.wordWrap = true;

        if (EditorApplication.isPlaying)
        {
            if (!DownloadManager.instance.IsDownloading)
            {
                if (EditorDataManager.Instance.EditScenario != null)
                {
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(450));
                    {
                        EditorGUILayout.BeginVertical(GUILayout.Width(150));
                        {
                            BuildScrollableOverview();
                        }
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.BeginVertical(GUILayout.MaxWidth(300));
                        {
                            BuildOverlayView();
                        }
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndHorizontal();

                    //if (GUILayout.Button("Save"))
                    //{
                    /*Debug.Log(selectedScenario);
                    PositionDataDataBase positiondatabase = new PositionDataDataBase(ScenarioDataBase.Instance.GetScenarioByID(selectedScenario).PositionDBID);

                    PositionData data = new PositionData();

                    if (OverlayDataBase.Instance.GetOverlayIDByName(target.name) == -1)
                    {
                        Debug.Log("overlay with name doesnt exist");
                        return;
                    }

                    data.OverlayID = OverlayDataBase.Instance.GetOverlayIDByName(target.name);
                    data.RectTransform = ((PositionSaver)target).GetComponent<RectTransform>().position;
                    data.RectDimentions = ((PositionSaver)target).GetComponent<RectTransform>().sizeDelta;
                    data.Rotation = ((PositionSaver)target).GetComponent<RectTransform>().rotation;
                    data.Scale = ((PositionSaver)target).GetComponent<RectTransform>().localScale;
                    if (positiondatabase.GetPositionDataByOverlayID(data.OverlayID) != null)
                    {
                        positiondatabase.UpdatePositionData(data);
                    }
                    else
                    {
                        positiondatabase.AddPositionData(data);
                    }
                    positiondatabase.Save();
                    */
                    //}
                }
                else
                {
                    GUILayout.Label("Select a scenario to start editing its overlays.");
                }
            }
            else
            {
                GUILayout.Label("Downloading, please wait...");
            }
        }
        else
        {
            GUILayout.Label("Enter play mode to start editing!");
        }
    }

    private void AddOverlayToScene(Overlay pOverlay)
    {
        GameObject parent = GameObject.Find("Overlays");
        GameObject overlayprefab = Resources.Load<GameObject>("GenericOverlay");
        GameObject instance = Instantiate<GameObject>(overlayprefab, Vector3.zero, Quaternion.identity, parent.transform);
        RectTransform instanceRect = instance.GetComponent<RectTransform>();
        instanceRect.name = pOverlay.Name;
        instanceRect.position = pOverlay.positions.RectTransform;
        instanceRect.sizeDelta = pOverlay.positions.RectDimentions;
        instanceRect.rotation = pOverlay.positions.Rotation;
        instanceRect.localScale = pOverlay.positions.Scale;
        Debug.Log(instance);
        gameobjectList.Add(instance);
    }

    private void RemoveOverlayFromScene(Overlay pOverlay)
    {
        GameObject overlayToDelete = gameobjectList.Find(X => X.name == pOverlay.Name);
        if (overlayToDelete == null) return;
        gameobjectList.Remove(overlayToDelete);
        GameObject.DestroyObject(overlayToDelete);
    }

    private void BuildScrollableOverview()
    {
        if (GUILayout.Button("New overlay", GUILayout.Width(142)))
        {
            AddNew();
            EndFocus();
        }

        scrollviewposition = EditorGUILayout.BeginScrollView(scrollviewposition);
        {
            foreach (Overlay overlay in overlayList)
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.button);
                if (GUILayout.Button(overlay.Name, GUILayout.Width(142)))
                {
                    editOverlay = overlay;
                    EndFocus();
                }
            }
        }
        EditorGUILayout.EndScrollView();
    }

    private void BuildOverlayView()
    {
        if (editOverlay != null)
        {
            EditorGUI.BeginDisabledGroup(true);
            {
                EditorGUILayout.TextField("Name: ", editOverlay.Name);
                EditorGUILayout.Vector3Field("Position", editOverlay.positions.RectTransform);
                EditorGUILayout.Vector2Field("Dimentions", editOverlay.positions.RectDimentions);
                EditorGUILayout.Vector3Field("Rotation", editOverlay.positions.Rotation.eulerAngles);
                EditorGUILayout.Vector3Field("Scale", editOverlay.positions.Scale);
            }
        }
    }

    private void AddNew()
    {
        // show popup with selection -> new or existing overlay
    }

    private void SaveEditedItem()
    {
        // only save in positionDataDataBase
    }

    private void DeleteEditedItem()
    {
        // only save in positionDataDataBase
    }

    private void EndFocus()
    {
        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }

    private void OnFocus()
    {
        //show elements
        foreach (GameObject gameObject in gameobjectList)
        {
            Image image = gameObject.GetComponent<Image>();
            Color color = image.color;
            color.a = 0.3f;
            image.color = color;
        }
    }

    private void OnLostFocus()
    {
        //hide elements
        foreach (GameObject gameObject in gameobjectList)
        {
            Image image = gameObject.GetComponent<Image>();
            Color color = image.color;
            color.a = 0.0f;
            image.color = color;
        }
    }
}

public class NewOverlayPopup : EditorWindow
{

    string _name = "";

    private void Awake()
    {
        //dataBase = ScenarioDataBase.Instance;
    }

    private void OnGUI()
    {
        GUILayout.Label("Create a new scenario");
        _name = EditorGUILayout.TextField("Scenario name:", _name);
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Create"))
            {
                //Scenario s = new Scenario(dataBase.GetUniqueID(), _name);
                //dataBase.AddScenario(s);
                //dataBase.Save();
                this.Close();
            }
            if (GUILayout.Button("Cancel"))
            {
                this.Close();
            }
        }
        GUILayout.EndHorizontal();
    }
}
