﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;

public class PresetEditor : EditorWindow {
    PresetDataBase presetDataBase;
    OverlayDataBase overlayDataBase;
    private int selectedID;
    Preset editPreset;
    Vector2 scrollviewposition = new Vector2();
    
    ScenarioDataBase scenarioDataBase;
    
    ReorderableList objectiveList;

    bool isInitialized = false;

    [MenuItem("VRRijschool/Preset Editor")]
    public static void ShowEditorWindow()
    {
        Type inspectorType = Type.GetType("UnityEditor.SceneView,UnityEditor.dll");
        GetWindow<PresetEditor>("Preset editor", true, inspectorType);
    }

    public void OnDownloaded(bool downloaded)
    {
        if (downloaded)
        {
            LoadData();
        }
    }

    private void LoadData()
    {
        presetDataBase = PresetDataBase.Instance;
        overlayDataBase = OverlayDataBase.Instance;
        scenarioDataBase = ScenarioDataBase.Instance;
        CountPresetUseage();
    }

    private void Initialize()
    {
        EditorApplication.playModeStateChanged += OnPlayModeChange;
        EditorDataManager.Instance.onDownloadsDone += OnDownloaded;
        LoadData();
        isInitialized = true;
    }
    
    public void OnPlayModeChange(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredPlayMode)
        {
            Initialize();
            Debug.Log(FindObjectOfType<DownloadForEditorScript>());
            FindObjectOfType<DownloadForEditorScript>().DownloadEverything();
        }
    }

    private void OnGUI()
    {
        if (!isInitialized)
            Initialize();
        if (EditorApplication.isPlaying)
        {
            if (!DownloadManager.instance.IsDownloading)
            {
                if (presetDataBase != null)
                {
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(450));
                    {
                        EditorGUILayout.BeginVertical(GUILayout.Width(150));
                        {
                            BuildScrollableOverview();
                        }
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.BeginVertical(GUILayout.MaxWidth(300));
                        {
                            BuildPresetView();
                        }
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndHorizontal();
                }
                else
                {
                    GUILayout.Label("preset database missing.");
                }
            }
            else
            {
                GUILayout.Label("Downloading, please wait...");
            }
        }
        else
        {
            GUILayout.Label("Enter play mode to start editing!");
        }
    }

    private void BuildScrollableOverview()
    {
        if (GUILayout.Button("New Preset"))
        {
            AddNew();
            EndFocus();
        }

        scrollviewposition = EditorGUILayout.BeginScrollView(scrollviewposition);
        {
            foreach (Preset preset in presetDataBase.GetPresets())
            {
                if (GUILayout.Button(preset.Name, GUILayout.Width(142)))
                {
                    editPreset = preset;
                    ObjectiveListSetup();
                    EndFocus();
                }
            }
        }
        EditorGUILayout.EndScrollView();
    }

    private void AddNew()
    {
        Preset newpreset = new Preset(presetDataBase.GetUniqueID());
        newpreset.Name = "New Preset";
        editPreset = newpreset;
        presetDataBase.AddPreset(newpreset);
    }
    
    private void SaveEditedItem()
    {
        presetDataBase.UpdatePreset(editPreset);
        presetDataBase.Save();
    }

    private void DeleteEditedItem()
    {
        presetDataBase.DeletePreset(editPreset);
        presetDataBase.Save();
    }

    private void EndFocus()
    {
        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }

    private void BuildPresetView()
    {
        if (editPreset != null)
        {
            editPreset.Name = EditorGUILayout.TextField("Name:", editPreset.Name);
            string text = "Preset is used: " + editPreset.Counter + " time";
            if (editPreset.Counter == 1) text += "s";
            text += ".";
            EditorGUILayout.LabelField(text);
            //special list thing

            objectiveList.DoLayoutList();

            GUILayout.Space(20);
            
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Save"))
                {
                    SaveEditedItem();
                    EndFocus();
                }
                using (new EditorGUI.DisabledScope(editPreset.Counter > 0))
                {
                    if (GUILayout.Button("Delete"))
                    {
                        DeleteEditedItem();
                        EndFocus();
                        editPreset = null;
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            EditorGUILayout.LabelField("Select a Preset on the left to get started!");
        }
    }

    private struct PresetAddParams
    {
        public Overlay oOverlay;
    }

    private void ObjectiveListSetup()
    {
        objectiveList = new ReorderableList(editPreset.GoalList, typeof(Preset), true, true, true, true);
        objectiveList.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Objectives");
        };
        objectiveList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocussed) =>
        {
            Overlay o = (Overlay)objectiveList.list[index];
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), o.Name);
        };
        objectiveList.onAddDropdownCallback = (Rect buttonrect, ReorderableList l) =>
        {
            GenericMenu menu = new GenericMenu();
            List<Overlay> menuitems = new List<Overlay>();
            menuitems = overlayDataBase.GetOverlays();
            foreach (Overlay overlay in menuitems)
            {
                menu.AddItem(new GUIContent(overlay.Name), false, clickHandler, new PresetAddParams { oOverlay = overlay });
            }
            menu.ShowAsContext();
        };
    }

    private void clickHandler(object target)
    {
        PresetAddParams data = (PresetAddParams)target;
        Overlay p = data.oOverlay;
        editPreset.GoalList.Add(p);
        SaveEditedItem();
    }

    private void CountPresetUseage()
    {
        foreach (Preset preset in presetDataBase.GetPresets())
        {
            preset.Counter = 0;
        }

        foreach (Scenario scenario in scenarioDataBase.GetScenarios())
        {
            scenario.LoadEventDB();
            foreach (Event eventt in scenario.EventDB.GetEvents())
            {
                if (eventt.EventType == Event.FrameEventType.Preset)
                {
                    Preset p = presetDataBase.GetPresetByID(eventt.PresetID);
                    if (p == null)
                    {
                        Debug.Log("Missing preset with ID: " + eventt.PresetID);
                    }
                    else
                    {
                        p.Counter++;
                    }
                }
            }
        }
    }

    
}
