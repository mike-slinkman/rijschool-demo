﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MainMenuEditor : EditorWindow {
    MenuDataBase database;
    Vector2 scrollviewposition = new Vector2();

    RijschoolMenuItem editItem;

    [MenuItem("VRRijschool/Main menu Editor")]
    public static void ShowEditorWindow()
    {
        Debug.Log("opening editor window");
        Type inspectorType = Type.GetType("UnityEditor.SceneView,UnityEditor.dll");
        GetWindow<MainMenuEditor>("Main menu editor", true, inspectorType);
    }

    private void OnGUI()
    {
        if (database == null)
        {
            database = MenuDataBase.Instance;
        }
        //force word wrap
        EditorStyles.textField.wordWrap = true;

        // {} between begin and end of areas is for ease of use. not required :)
        EditorGUILayout.BeginHorizontal(GUILayout.Width(450));
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(150));
            {
                BuildScrollableOverview();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(300));
            {
                BuildEditView();
            }
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.EndHorizontal();
    }

    private void BuildScrollableOverview()
    {
        if (GUILayout.Button("New Item", GUILayout.Width(142)))
        {
            AddItem();
            EndFocus();
        }

        scrollviewposition = EditorGUILayout.BeginScrollView(scrollviewposition);
        {
            List<RijschoolMenuItem> menuItemList = database.GetMenuItems();
            foreach (RijschoolMenuItem _menuitem in menuItemList)
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.button);
                if (editItem != null && _menuitem.ID == editItem.ID)
                {
                    gUIStyle.normal.background = MakeTex(new Color(0.0f, 1.0f, 0.0f));
                }
                if (GUILayout.Button(_menuitem.ID.ToString() + " " + _menuitem.Name, gUIStyle, GUILayout.Width(142)))
                {
                    editItem = _menuitem;
                    EndFocus();
                }
            }
        }
        EditorGUILayout.EndScrollView();
    }

    private void BuildEditView()
    {
        if (editItem != null)
        {
            EditorGUI.BeginDisabledGroup(editItem.IsRoot == true);
            {
                float oldlabelwidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = 125;

                //parent, child, select action
                editItem.Name = EditorGUILayout.TextField("Name:", editItem.Name);
                EditorGUILayout.PrefixLabel("Description:");
                editItem.Description = EditorGUILayout.TextArea(editItem.Description, GUILayout.Height(100), GUILayout.Width(300));
                editItem.Icon = EditorGUILayout.TextField("Icon:", editItem.Icon);
                List<RijschoolMenuItem> possibleParentList = new List<RijschoolMenuItem>();
                RijschoolMenuItem rootitem = new RijschoolMenuItem();
                rootitem.Name = "None";
                possibleParentList.Add(rootitem);
                Debug.Log("total objects: " + database.GetMenuItems().Count);
                possibleParentList.AddRange(database.GetMenuItems().FindAll(X => X.ID != editItem.ID &&
                                            (X.parentID != editItem.ID || X.parentID == 0) &&
                                            (X.selectAction == RijschoolMenuItem.SelectAction.MenuLayer)));
                Debug.Log("possible parents: " + possibleParentList.Count);
                int parentIndex = 0;
                if (editItem.HasParent())
                    parentIndex = possibleParentList.FindIndex(X => X.ID == editItem.parentID);
                int newParentIndex = EditorGUILayout.Popup("Parent: ", parentIndex, possibleParentList.Select(X => X.Name).ToArray(), EditorStyles.popup);
                if (parentIndex != newParentIndex)
                {
                    if (parentIndex != 0)
                    {
                        possibleParentList[parentIndex].RemoveChild(editItem);
                    }
                    if (newParentIndex != 0)
                    {
                        possibleParentList[newParentIndex].SetChild(editItem);
                    }
                }
            }
            EditorGUI.EndDisabledGroup();

            if (editItem.selectAction == RijschoolMenuItem.SelectAction.MenuLayer)
            {
                //tijdelijke children info
                if (editItem.children != null)
                    EditorGUILayout.LabelField("children:", editItem.children.Count.ToString());
                else
                    EditorGUILayout.LabelField("children:", "Null");
            }
            else
            {
                GUILayout.Space(18);
            }

            EditorGUI.BeginDisabledGroup(editItem.IsRoot == true);
            { 
                //tijdelijke select action info
                //EditorGUILayout.LabelField("select action:", editItem.selectAction.ToString());
                editItem.selectAction = (RijschoolMenuItem.SelectAction)EditorGUILayout.EnumPopup("Select action:", editItem.selectAction);

                if (editItem.selectAction == RijschoolMenuItem.SelectAction.PlayVideo)
                {
                    //show scenario selector
                    //EditorGUILayout.LabelField("Linked scenario:", editItem.ScenarioID.ToString());
                    List<Scenario> PossibleLinkedScenarios = new List<Scenario>();
                    Scenario dummy = new Scenario(0, "None");
                    PossibleLinkedScenarios.Add(dummy);
                    PossibleLinkedScenarios.AddRange(ScenarioDataBase.Instance.GetScenarios());
                    //Debug.Log("possible scenarios before removing dupes: " + PossibleLinkedScenarios.Count);
                    foreach (RijschoolMenuItem item in MenuDataBase.Instance.GetMenuItems())
                    {
                        if (item.selectAction == RijschoolMenuItem.SelectAction.PlayVideo)
                        {
                            Scenario removescenario = PossibleLinkedScenarios.Find(X => X.ID == item.ScenarioID);
                            if (removescenario != null && removescenario.ID != editItem.ScenarioID)
                            {
                                PossibleLinkedScenarios.Remove(removescenario);
                            }
                        }
                    }
                    int scenarioindex = 0;
                    if (editItem.ScenarioID != 0)
                    {
                        scenarioindex = PossibleLinkedScenarios.FindIndex(X => X.ID == editItem.ScenarioID);
                    }
                    scenarioindex = EditorGUILayout.Popup("Scenario: ", scenarioindex, PossibleLinkedScenarios.Select(X => X.Name).ToArray(), EditorStyles.popup);
                    editItem.ScenarioID = PossibleLinkedScenarios[scenarioindex].ID;
                }

                EditorGUILayout.LabelField("Root item:", editItem.IsRoot.ToString());
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.Space(16);
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Save"))
                {
                    SaveEditItem();
                    EndFocus();
                }
                if (GUILayout.Button("Delete"))
                {
                    DeleteEditItem();
                    EndFocus();
                    editItem = null;
                }
            }
        }
        else
        {
            EditorGUILayout.LabelField("Select a menu item on the left to get started!");
        }
    }

    private void AddItem()
    {
        RijschoolMenuItem rijschoolMenuItem = new RijschoolMenuItem();
        rijschoolMenuItem.ID = database.GetUniqueID();
        if (database.GetRootMenuItem() == null)
        {
            rijschoolMenuItem.IsRoot = true;
            rijschoolMenuItem.Name = "Root";
        }
        database.AddMenuItem(rijschoolMenuItem);
    }

    private void SaveEditItem()
    {
        database.UpdateMenuItem(editItem);
        database.Save();
    }

    private void DeleteEditItem()
    {
        database.DeleteMenuItem(editItem);
        database.Save();
    }

    private void EndFocus()
    {
        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }

    /// <summary>
    /// makes a 1x1 texture in the specified color
    /// </summary>
    /// <param name="col">color of the texture</param>
    /// <returns>Texture2D in specified color</returns>
    private Texture2D MakeTex(Color col)
    {
        Color[] pix = new Color[1 * 1];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(1, 1);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }
}
