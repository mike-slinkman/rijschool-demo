﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;

public class EventEditor : EditorWindow
{

#region Variables
    EventDataBase eventDataBase;
    PresetDataBase presetDataBase;
    Scenario parent;
    private string databaseID;
    Event editEvent;
    QuizAnswer editQuizAnswer;
    int editQuizAnswerID;
    Vector2 scrollviewposition = new Vector2();
    Vector2 questionscrollviewposition = new Vector2();
    int presetindex;

    ReorderableList objectiveList;

    bool isInitialized = false;
    #endregion

    [MenuItem("VRRijschool/Event editor")]
    public static void ShowEditorWindow()
    {
        GetWindow<EventEditor>("Event editor", true);
    }

    private void Initialize()
    {
        if (Application.isPlaying)
        {
            EditorApplication.playModeStateChanged += OnPlayModeChange;
            EditorDataManager.Instance.onEditScenarioChange += LoadNewScenario;
            isInitialized = true;
        }
    }

    public void OnPlayModeChange(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredPlayMode)
        {
            Initialize();
        }
    }

    /// <summary>
    /// sets up the window with the specified eventDatabaseID
    /// </summary>
    /// <param name="Scenario">chosen scenario</param>
    private void LoadNewScenario(Scenario Scenario)
    {
        Debug.Log("new scenario selected, loading it now");
        databaseID = Scenario.EventDBID;
        parent = Scenario;
        eventDataBase = new EventDataBase(databaseID);
        presetDataBase = PresetDataBase.Instance;
        presetindex = 0;
        editEvent = null;
        editQuizAnswer = null;
        editQuizAnswerID = 0;
        scrollviewposition = new Vector2();
        questionscrollviewposition = new Vector2();
        Repaint();
    }

    private void OnGUI()
    {
        if (!isInitialized)
            Initialize();
        if (Application.isPlaying)
        {
            if (!DownloadManager.instance.IsDownloading)
            {
                if (EditorDataManager.Instance.EditScenario != null)
                {
                    if (databaseID == null && EditorDataManager.Instance.EditScenario != null ||
                        eventDataBase == null && EditorDataManager.Instance.EditScenario != null)
                    {
                        Debug.Log(EditorDataManager.Instance.EditScenario);
                        LoadNewScenario(EditorDataManager.Instance.EditScenario);
                    }
                    //force word wrap
                    EditorStyles.textField.wordWrap = true;

                    // {} between begin and end of areas is for ease of use. not required :)
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(450));
                    {
                        EditorGUILayout.BeginVertical(GUILayout.Width(150));
                        {
                            BuildScrollableOverview();
                        }
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.BeginVertical(GUILayout.MaxWidth(300));
                        {
                            BuildEventView();
                        }
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndHorizontal();
                }
                else
                {
                    GUILayout.Label("Select a scenario to start editing its events.");
                }
            }
            else
            {
                GUILayout.Label("Downloading, please wait...");
            }
        }
        else
        {
            GUILayout.Label("Enter play mode to start editing!");
        }
    }

    #region Gui building
    /// <summary>
    /// Builds the list of events visible on the left side of the screen.
    /// </summary>
    private void BuildScrollableOverview()
    {
        if (GUILayout.Button("New Event", GUILayout.Width(142)))
        {
            AddNew();
            EndFocus();
        }

        scrollviewposition = EditorGUILayout.BeginScrollView(scrollviewposition);
        {
            List<Event> eventlist = eventDataBase.GetEvents();
            eventlist = eventlist.OrderBy(X => X.Frame).ToList();
            foreach (Event _event in eventlist)
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.button);
                if (GUILayout.Button(_event.Header, GUILayout.Width(142)))
                {
                    editEvent = _event;
                    ObjectiveListSetup();
                    EndFocus();
                }
            }
        }
        EditorGUILayout.EndScrollView();
    }

    /// <summary>
    /// builds the generic part of the event view.
    /// </summary>
    private void BuildEventView()
    {
        if (editEvent != null)
        {
            float oldlabelwidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 125;
            EditorGUILayout.LabelField("Current Event:", editEvent.ID.ToString());
            editEvent.Frame = EditorGUILayout.IntField("Starting frame:", editEvent.Frame);
            if (editEvent.EndFrame < editEvent.Frame) editEvent.EndFrame = editEvent.Frame + 100;
            editEvent.EndFrame = EditorGUILayout.IntField("End frame:", editEvent.EndFrame);
            editEvent.EventType = (Event.FrameEventType)EditorGUILayout.EnumPopup("Event type:", editEvent.EventType);
            
            if (editEvent.EventType == Event.FrameEventType.Quiz)
            {
                BuildQuiz();
            }
            else if (editEvent.EventType == Event.FrameEventType.Custom)
            {
                BuildGenericForGoals();
                BuildObjectives();
            }
            else if (editEvent.EventType == Event.FrameEventType.Preset)
            {
                BuildGenericForGoals();
                BuildPresetSelector();
            }
            else
            {
                BuildContinueButton();
            }

            GUILayout.Space(16);
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Save"))
                {
                    SaveEditedItem();
                    EndFocus();
                }
                if (GUILayout.Button("Delete"))
                {
                    DeleteEditedItem();
                    EndFocus();
                    editEvent = null;
                }
            }

            EditorGUIUtility.labelWidth = oldlabelwidth;
        }
        else
        {
            EditorGUILayout.LabelField("Select a Event on the left to get started!");
        }
    }

    /// <summary>
    /// builds the continue button specific elements
    /// </summary>
    private void BuildContinueButton()
    {
        //editEvent.FreezeVideo = EditorGUILayout.Toggle("Pause video on event start:", editEvent.FreezeVideo);
        //editEvent.IsPopUp = EditorGUILayout.Toggle("Is this event a popup:", editEvent.IsPopUp);
        
        editEvent.ImageName = EditorGUILayout.TextField("Header image name:", editEvent.ImageName, GUILayout.Width(300));
        editEvent.Header = EditorGUILayout.TextField("Header text:", editEvent.Header, GUILayout.Width(300));
        //EditorGUILayout.BeginHorizontal();
        //{
            EditorGUILayout.PrefixLabel("Event message:");
            editEvent.Message = EditorGUILayout.TextArea(editEvent.Message, GUILayout.Height(100), GUILayout.Width(300));
        //}
        //EditorGUILayout.EndHorizontal();
    }

    /// <summary>
    /// builds the quiz specific elements.
    /// </summary>
    private void BuildQuiz()
    {
        //show question
        editEvent.Header = EditorGUILayout.TextField("Header text:", editEvent.Header);
        //EditorGUILayout.BeginHorizontal();
        //{
            EditorGUILayout.PrefixLabel("Quiz question:");
            editEvent.Message = EditorGUILayout.TextArea(editEvent.Message, GUILayout.Height(100), GUILayout.Width(300));
        //}
        //EditorGUILayout.EndHorizontal();

        GUIStyle gUIStyle = new GUIStyle();
        gUIStyle.normal.background = MakeTex(new Color(0.6f, 0.6f, 0.6f, 1f));
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.BeginVertical(gUIStyle);
        { 
            EditorGUILayout.LabelField("Answers:");
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical();
                {
                    if (GUILayout.Button("Add answer"))
                    {
                        QuizAnswer a = new QuizAnswer("", false);
                        editEvent.QuizAnswers.Add(a);
                        SaveEditedItem();
                        editQuizAnswer = a;
                        editQuizAnswerID = editEvent.QuizAnswers.Count;
                    }
                    questionscrollviewposition = EditorGUILayout.BeginScrollView(questionscrollviewposition);
                    {
                        for (int i = 0; i < editEvent.QuizAnswers.Count; i++)
                        {
                            if (GUILayout.Button((i + 1) + " " + editEvent.QuizAnswers[i].AnswerText))
                            {
                                editQuizAnswer = editEvent.QuizAnswers[i];
                                editQuizAnswerID = i;
                            }
                        }
                    }
                    EditorGUILayout.EndScrollView();
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical();
                {
                    if (editQuizAnswer != null)
                    {
                        EditorGUILayout.LabelField("editing answer:", (editQuizAnswerID + 1).ToString());
                        editQuizAnswer.AnswerText = EditorGUILayout.TextField("Answer:", editQuizAnswer.AnswerText);
                        editQuizAnswer.IsCorrect = EditorGUILayout.Toggle("Correct:", editQuizAnswer.IsCorrect);
                        EditorGUILayout.BeginHorizontal();
                        {
                            if (GUILayout.Button("Save"))
                            {
                                editEvent.QuizAnswers[editQuizAnswerID].AnswerText = editQuizAnswer.AnswerText;
                                editEvent.QuizAnswers[editQuizAnswerID].IsCorrect = editQuizAnswer.IsCorrect;
                                SaveEditedItem();
                            }
                            if (GUILayout.Button("Delete"))
                            {
                                editEvent.QuizAnswers.RemoveAt(editQuizAnswerID);
                                SaveEditedItem();
                                editQuizAnswer = null;
                                editQuizAnswerID = 0;
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();

        //show answer list
    }

    /// <summary>
    /// Builds the generic ui for setting goals.
    /// </summary>
    private void BuildGenericForGoals()
    {
        editEvent.ImageName = EditorGUILayout.TextField("Header image name:", editEvent.ImageName);
        editEvent.Header = EditorGUILayout.TextField("Header text:", editEvent.Header);
        //EditorGUILayout.BeginHorizontal();
        //{
            EditorGUILayout.PrefixLabel("Event message:");
            editEvent.Message = EditorGUILayout.TextArea(editEvent.Message, GUILayout.Height(100), GUILayout.Width(300));
        //}
        //EditorGUILayout.EndHorizontal();

        //do slider 1 and move slider 2 if it gets below the value of slider 1
        editEvent.SkillThresholdMedium = EditorGUILayout.IntSlider("Medium difficulty treshold:", editEvent.SkillThresholdMedium, 1, 100);
        if (editEvent.SkillThresholdMedium > 99) editEvent.SkillThresholdMedium = 99;
        if (editEvent.SkillThresholdHard <= editEvent.SkillThresholdMedium) editEvent.SkillThresholdHard = editEvent.SkillThresholdMedium + 1;

        //do slider 2 and move slider 1 if it gets above the value of slider 2
        editEvent.SkillThresholdHard = EditorGUILayout.IntSlider("Hard difficulty treshold:", editEvent.SkillThresholdHard, 1, 100);
        if (editEvent.SkillThresholdHard < 2) editEvent.SkillThresholdHard = 2;
        if (editEvent.SkillThresholdMedium >= editEvent.SkillThresholdHard) editEvent.SkillThresholdMedium = editEvent.SkillThresholdHard - 1;
    }

    /// <summary>
    /// Builds the preset dropdown list.
    /// </summary>
    private void BuildPresetSelector()
    {
        List<Preset> presets = new List<Preset>(presetDataBase.GetPresets());
        Debug.Log("Loaded: " + presets.Count + " presets");
        parent.LoadPositionsDB();
        List<Overlay> overlays = OverlayDataBase.Instance.GetOverlayListForPositions(parent.PositionDB.GetPositionDatas());
        for (int i = presets.Count - 1; i >= 0; i--)
        {
            bool keep = true;
            foreach (Overlay overlay in presets[i].GoalList)
            {
                if (overlays.Find(X => X.ID == overlay.ID) == null)
                    keep = false;
            }
            if (!keep)
            {
                presets.Remove(presets[i]);
                Debug.Log("removed a preset");
            }
        }

        presetindex = presets.FindIndex(X => X.ID == editEvent.PresetID);
        presetindex = (presetindex == -1 ? 0 : presetindex);
        presetindex = EditorGUILayout.Popup("preset:", presetindex, presets.Select(X => X.Name).ToArray(), EditorStyles.popup);
        Debug.Log("presets contains: " + presets.Count + " presets. index is at: " +  presetindex);
        editEvent.PresetID = presets[presetindex].ID;
    }

    /// <summary>
    /// builds the objective specific elements
    /// </summary>
    private void BuildObjectives()
    {
        //create list of current goals:
        objectiveList.DoLayoutList();
    }
    #endregion

    #region Sortable list code.
    private struct OverlayAddParams
    {
        public Overlay oOverlay;
    }

    private void ObjectiveListSetup()
    {
        objectiveList = new ReorderableList(editEvent.GoalList, typeof(Overlay), true, true, true, true);
        objectiveList.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "Objectives");
        };
        /*objectiveList.onAddCallback = (ReorderableList l) => {
            int index = l.list.Count;
            l.index = i;
        };*/
        objectiveList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocussed) =>
        {
            Overlay overlay = (Overlay)objectiveList.list[index];
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), overlay.Name);
        };
        objectiveList.onAddDropdownCallback = (Rect buttonRect, ReorderableList l) =>
        {
            parent.LoadPositionsDB();
            GenericMenu menu = new GenericMenu();
            List<Overlay> menuitems = new List<Overlay>();
            menuitems = OverlayDataBase.Instance.GetOverlayListForPositions(parent.PositionDB.GetPositionDatas());
            foreach (Overlay overlay in menuitems)
            {
                menu.AddItem(new GUIContent(overlay.Name), false, clickHandler, new OverlayAddParams { oOverlay = overlay });
            }
            menu.ShowAsContext();
        };
    }

    private void clickHandler(object target)
    {
        OverlayAddParams data = (OverlayAddParams)target;
        Overlay overlay = data.oOverlay;
        editEvent.GoalList.Add(overlay);
        SaveEditedItem();
    }
    #endregion

    #region Crud functions
    private void AddNew()
    {
        Event newEvent = new Event(eventDataBase.GetUniqueID(), 0);
        newEvent.Frame = 0;
        eventDataBase.AddEvent(newEvent);
    }

    private void SaveEditedItem()
    {
        eventDataBase.UpdateEvent(editEvent);
        eventDataBase.Save();
    }

    private void DeleteEditedItem()
    {
        eventDataBase.DeleteEvent(editEvent);
        eventDataBase.Save();
    }

    private void EndFocus()
    {
        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }
    #endregion

    /// <summary>
    /// makes a 1x1 texture in the specified color
    /// </summary>
    /// <param name="col">color of the texture</param>
    /// <returns>Texture2D in specified color</returns>
    private Texture2D MakeTex(Color col)
    {
        Color[] pix = new Color[1 * 1];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(1, 1);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }
}
