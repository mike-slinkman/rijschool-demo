﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OldMenuManager : MonoBehaviour {

    [SerializeField]
    private List<FillButtonScript> subjectBtns = new List<FillButtonScript>();

    [SerializeField]
    private FadeScript fadeScript;

    void Start()
    {
        foreach(FillButtonScript btn in subjectBtns)
        {
            btn.onfillCompleted += SubjectChosen;
        }   
    }

    void SubjectChosen(GameObject obj, string subjName)
    {
        fadeScript.FadeOutScene();
        SceneManager.LoadSceneAsync(subjName);
    }

}
