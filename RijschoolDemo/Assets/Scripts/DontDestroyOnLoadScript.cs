﻿using UnityEngine;

public class DontDestroyOnLoadScript : MonoBehaviour {

    // Use this for initialization
    public void Awake()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
    
    public void DontDestroy()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
